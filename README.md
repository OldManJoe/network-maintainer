Network Maintainer (Name Pending)

This program is designed to help people keep in touch with their friends and business associates on a pseudorandomly regular basis

This program stores a list of contacts that it randomly assigns a date of contact to within given parameters that allow you to keep in contact with people that you may not see everyday to help you keep in contact. You assign each contact an interval, there are a number of prebuilt intervals: weekly, biweekly (once every two weeks), monthly, bimonthly (once every two months), quarterly (once every three months), triannually (once every four months), semiannually (once every six months), or annually. Also, users can assign a custom interval of days for a contact.

The main screen shows a prompt to send to each contact that can be refreshed with a button press. Mark each of today's contacts as contacted after you have reached out to them, or you can reschedule them to another day within the current time period if you want to reach out to them later, just don't procrastinate too much. The program will do it's best to keep each day as even as possible which can cause later days in the month to become quite populated if you miss days.

Each contact stores their name, username, social network, contact dates, contact interval, and notes. You can also mark a contact as disabled so that they are removed from the calendar but kept in the system to be re-enabled later, but you can also delete a contact you no longer wish to reach out to.