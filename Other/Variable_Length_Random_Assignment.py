# example file for how to do random assignment to different days

import random

annual = ["1Y"] * 10
semiannual = ["2Y"] * 10
bimonthly = ["2M"] * 30
monthly = ["1M"] * 150
biweekly = ["2W"] * 20
weekly = ["1W"] * 20

total_assignments = len(annual) + len(semiannual) + len(bimonthly) + len(monthly) + len(biweekly) + len(weekly)

assignments = []
num_days = 366 * 2

def assign_list(start_index, length, contact_list) :
    global assignments
    
    min_start = len(assignments[(start_index) % num_days])
    min_assigned = min_start
    possible_indicies_base = []
    possible_indicies = []

    for a in range(length) :
        possible_indicies_base.append(a)

    for a in range(length) :
        if len(assignments[(start_index+a) % num_days]) < min_assigned :
            possible_indicies = [a]
            min_assigned = len(assignments[(start_index+a) % num_days])
        elif len(assignments[(start_index+a) % num_days]) == min_assigned :
            possible_indicies.append(a)

    rand_index = 0
    for contact in contact_list :
        rand_index = possible_indicies[random.randint(0, len(possible_indicies) - 1)]
        assignments[(start_index+rand_index) % num_days].append(contact)
        possible_indicies.remove(rand_index)
        if len(possible_indicies) == 0:
            possible_indicies = possible_indicies_base.copy()
        else :
            continue

def assign_single(start_index, length, contact) :
    global assignments
    
    min_start = len(assignments[start_index % num_days])
    min_assigned = min_start
    possible_indicies_base = []
    possible_indicies = []

    for a in range(length) :
        possible_indicies_base.append(a)

    for a in range(length) :
        if len(assignments[(start_index+a) % num_days]) < min_assigned :
            possible_indicies = [a]
            min_assigned = len(assignments[(start_index+a) % num_days])
        elif len(assignments[(start_index+a) % num_days]) == min_assigned :
            possible_indicies.append(a)

    rand_index = possible_indicies[random.randint(0, len(possible_indicies) - 1)]
    assignments[(start_index+rand_index) % num_days].append(contact)
    possible_indicies.remove(rand_index)

def reassign_day(day, contact_list) :    
    for contact in contact_list :
        if contact == "1Y" :
            mod = day // 366 + 1
            assign_single(366 * mod, 366, contact)
        elif contact == "2Y" :
            mod = day // 183 + 1
            assign_single(183 * mod, 183, contact)
        elif contact == "2M" :
            mod = day // 60 + 1
            assign_single(60 * mod, 60, contact)
        elif contact == "1M" :
            mod = day // 30 + 1
            assign_single(30 * mod, 30, contact)
        elif contact == "2W" :
            mod = day // 14 + 1
            assign_single(14 * mod, 14, contact)
        elif contact == "1W" :
            mod = day // 7 + 1
            assign_single(7 * mod, 7, contact)

def main () :
    print("This program is an example for randomly assigning people to a day for contacting and " +
          "then randomly reassigning them to a new day after contacting them. people are reassigned " +
          "to a day within the next block of dates, so each week is considered as separate block. " +
          "Individuals are assigned based on their frequency. \n" +
          "1W is weekly, 2W is biweekly \n" +
          "1M is monthly, 2M is bimonthly \n" +
          "2Y is semiannually, 1Y is annually \n" +
          "The number at the beginning of each row is the 'date' \n" +
          "The first list printed is the original assignments, then the total number of people assigned. \n" +
          "Then the second list is after a specified number of steps forward. \n\n\n" +
          "Blocking is the spaces between time periods, weekly will have many blank lines")

    blocking = input("How would you like to show blocking? Weekly, Monthly, or None? Enter [W, M, or N]: ")

    if (blocking == "W" or blocking == "w" ) : blocking = 7
    elif (blocking == "M" or blocking == "m") : blocking = 30
    else : blocking = 0

    for a in range(num_days) :
        assignments.append([])

    assign_list(0, 366, annual)
    assign_list(0, 183, semiannual)
    assign_list(0, 60, bimonthly)
    assign_list(0, 30, monthly)
    assign_list(0, 14, biweekly)
    assign_list(0, 7, weekly)

    a = 0
    num = 0
    while a < (num_days) :
        if assignments[a] :
            num += len(assignments[a])
            print(a+1, assignments[a])
        a += 1
        if (blocking != 0 and a % blocking == 0) :
            print()

    print("Number assigned {} should equal {}".format(num, total_assignments))

    current_day = 0
    while (True) :
        print("Current day is {}".format(current_day+1))
        step_forward = int(input("How many days would you like to step forward? (0 for quit) "))

        if step_forward == 0 : return
        
        for b in range(step_forward) :
            reassign_day(current_day+b, assignments[(current_day+b) % num_days])
            assignments[(current_day+b) % num_days] = []

        current_day += step_forward
        num = 0
        for b in range(len(assignments)) :
            if assignments[(current_day+b) % num_days] :
                num += len(assignments[(current_day+b) % num_days])
                print(current_day+b+1, assignments[(current_day+b) % num_days])
            if (blocking != 0 and (current_day+b+1) % blocking == 0) :
                print()

    print("Number assigned {} should equal {}".format(num, total_assignments))

main()
