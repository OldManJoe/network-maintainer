/*
 * MyPanel_Contact_Today.cpp
 *
 *  Created on: Dec 21, 2023
 *      Author: oldma
 */

#include "wxPanel_Contact_Today.h"

#include "Calendar_Manager.h"
#include "File_Manager.h"
#include "Global_Variables.h"
#include "Window_Manager.h"

using std::vector;
using std::string;

wxPanel_Contact_Today::wxPanel_Contact_Today(wxWindow * parent) : wxPanel(parent, wxID_ANY)
{
	int spacer_width = 65;

	this->SetSizeHints( wxSize( 800,600 ), wxDefaultSize );

	wxGridBagSizer* gbSizer1;
	gbSizer1 = new wxGridBagSizer( 0, 0 );
	gbSizer1->SetFlexibleDirection( wxBOTH );
	gbSizer1->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	m_staticText1 = new wxStaticText( this, wxID_ANY, wxT("Prompt:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText1->Wrap( -1 );
	gbSizer1->Add( m_staticText1, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5 );

	m_textCtrl_Prompt = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_READONLY );
	gbSizer1->Add( m_textCtrl_Prompt, wxGBPosition( 0, 1 ), wxGBSpan( 1, 7 ), wxALIGN_CENTER_VERTICAL|wxALL|wxEXPAND, 5 );

	m_button_Prompt = new wxButton( this, wxID_ANY, wxT("Refresh Prompt"), wxDefaultPosition, wxDefaultSize, 0 );
	gbSizer1->Add( m_button_Prompt, wxGBPosition( 0, 8 ), wxGBSpan( 1, 1 ), wxALIGN_CENTER|wxALL, 5 );

	m_button_All = new wxButton( this, wxID_ANY, wxT("Select All"), wxDefaultPosition, wxDefaultSize, 0 );
	gbSizer1->Add( m_button_All, wxGBPosition( 1, 1 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	gbSizer1->Add( spacer_width, 0, wxGBPosition( 1, 2 ), wxGBSpan( 1, 1 ), wxEXPAND, 5 );

	m_button_Contacted = new wxButton( this, wxID_ANY, wxT("Contacted"), wxDefaultPosition, wxDefaultSize, 0 );
	gbSizer1->Add( m_button_Contacted, wxGBPosition( 1, 3 ), wxGBSpan( 2, 1 ), wxALL|wxALIGN_CENTER_VERTICAL|wxALIGN_CENTER_HORIZONTAL, 5 );
	m_button_Contacted->Enable(false);

	gbSizer1->Add( spacer_width, 0, wxGBPosition( 1, 4 ), wxGBSpan( 1, 1 ), wxEXPAND, 5 );

	m_button_Reschedule = new wxButton( this, wxID_ANY, wxT("Reschedule"), wxDefaultPosition, wxDefaultSize, 0 );
	gbSizer1->Add( m_button_Reschedule, wxGBPosition( 1, 5 ), wxGBSpan( 2, 1 ), wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5 );
	m_button_Reschedule->Enable(false);

	gbSizer1->Add( spacer_width, 0, wxGBPosition( 1, 6 ), wxGBSpan( 1, 1 ), wxEXPAND, 5 );

	m_button_Details = new wxButton( this, wxID_ANY, wxT("Details"), wxDefaultPosition, wxDefaultSize, 0 );
	gbSizer1->Add( m_button_Details, wxGBPosition( 1, 7 ), wxGBSpan( 2, 1 ), wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5 );
	m_button_Details->Enable(false);

	m_dataViewListCtrl_Daily_Contacts = new wxDataViewListCtrl( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxDV_MULTIPLE|wxFULL_REPAINT_ON_RESIZE );
	m_dataViewListCtrl_Daily_Contacts->SetMinSize( wxSize( 775,400 ) );

	m_dataViewListColumn1 = m_dataViewListCtrl_Daily_Contacts->AppendTextColumn( wxT("Name"), wxDATAVIEW_CELL_INERT, 300, static_cast<wxAlignment>(wxALIGN_LEFT), wxDATAVIEW_COL_RESIZABLE );
	m_dataViewListColumn2 = m_dataViewListCtrl_Daily_Contacts->AppendTextColumn( wxT("Username"), wxDATAVIEW_CELL_INERT, 300, static_cast<wxAlignment>(wxALIGN_LEFT), wxDATAVIEW_COL_RESIZABLE );
	m_dataViewListColumn3 = m_dataViewListCtrl_Daily_Contacts->AppendTextColumn( wxT("Social Network"), wxDATAVIEW_CELL_INERT, 100, static_cast<wxAlignment>(wxALIGN_LEFT), wxDATAVIEW_COL_RESIZABLE );
	gbSizer1->Add( m_dataViewListCtrl_Daily_Contacts, wxGBPosition( 3, 0 ), wxGBSpan( 1, 9 ), wxALL, 5 );

	m_button_New_Contact = new wxButton( this, wxID_ANY, wxT("New Contact"), wxDefaultPosition, wxDefaultSize, 0 );
	gbSizer1->Add( m_button_New_Contact, wxGBPosition( 4, 1 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	m_button_All_Contacts = new wxButton( this, wxID_ANY, wxT("All Contacts"), wxDefaultPosition, wxDefaultSize, 0 );
	gbSizer1->Add( m_button_All_Contacts, wxGBPosition( 4, 4 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	m_button_Social_Networks = new wxButton( this, wxID_ANY, wxT("Social Networks"), wxDefaultPosition, wxDefaultSize, 0 );
	gbSizer1->Add( m_button_Social_Networks, wxGBPosition( 4, 7 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	this->SetSizer( gbSizer1 );
	this->Layout();

	this->Centre( wxBOTH );

	// Connect Events
	m_button_Prompt->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_Contact_Today::Refresh_Prompt ), NULL, this );
	m_button_All->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_Contact_Today::Select_All ), NULL, this );
	m_button_Contacted->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_Contact_Today::Contacted ), NULL, this );
	m_button_Reschedule->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_Contact_Today::Reschedule ), NULL, this );
	m_button_Details->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_Contact_Today::Details ), NULL, this );
	m_button_New_Contact->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_Contact_Today::New_Contact ), NULL, this );
	m_button_All_Contacts->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_Contact_Today::All_Contacts ), NULL, this );
	m_button_Social_Networks->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_Contact_Today::Social_Networks ), NULL, this );
	m_dataViewListCtrl_Daily_Contacts->Connect( wxEVT_DATAVIEW_SELECTION_CHANGED, wxCommandEventHandler( wxPanel_Contact_Today::Selection_Changes ), NULL, this );

	build();
}

wxPanel_Contact_Today::~wxPanel_Contact_Today()
{
	// Disconnect Events
	m_button_Prompt->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_Contact_Today::Refresh_Prompt ), NULL, this );
	m_button_All->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_Contact_Today::Select_All ), NULL, this );
	m_button_Contacted->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_Contact_Today::Contacted ), NULL, this );
	m_button_Reschedule->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_Contact_Today::Reschedule ), NULL, this );
	m_button_Details->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_Contact_Today::Details ), NULL, this );
	m_button_New_Contact->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_Contact_Today::New_Contact ), NULL, this );
	m_button_All_Contacts->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_Contact_Today::All_Contacts ), NULL, this );
	m_button_Social_Networks->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_Contact_Today::Social_Networks ), NULL, this );
	m_dataViewListCtrl_Daily_Contacts->Disconnect( wxEVT_DATAVIEW_SELECTION_CHANGED, wxCommandEventHandler( wxPanel_Contact_Today::Selection_Changes ), NULL, this );
}

void wxPanel_Contact_Today::build() {
	contact_uuids = Calendar_Manager::contact_today();
	greetings = File_Manager::get_greetings();
	questions = File_Manager::get_questions();

	Contact contact;
	wxVector<wxVariant> contact_data;
	for (string uuid : contact_uuids) {
		contact = File_Manager::get_contact(uuid);
		contact_data.clear();
		contact_data.push_back(contact.get_name());
		contact_data.push_back(contact.username);
		contact_data.push_back(contact.get_social_network_name());
		m_dataViewListCtrl_Daily_Contacts->AppendItem(contact_data);
	}

	wxCommandEvent null_event; Refresh_Prompt(null_event);
}

void wxPanel_Contact_Today::Refresh_Prompt( wxCommandEvent& event ) {
	std::uniform_int_distribution greetings_uid(0, static_cast<int>(greetings.size() - 1));
	std::uniform_int_distribution questions_uid(0, static_cast<int>(questions.size() - 1));

	string prompt = greetings[greetings_uid(mt)] + " " + questions[questions_uid(mt)];
	m_textCtrl_Prompt->ChangeValue(prompt);
}

void wxPanel_Contact_Today::Select_All( wxCommandEvent& event ) {
	m_dataViewListCtrl_Daily_Contacts->SelectAll();
	enable_buttons_check();
}

void wxPanel_Contact_Today::Contacted( wxCommandEvent& event ) {
	if (m_dataViewListCtrl_Daily_Contacts->GetSelectedItemsCount() > 1) {
		for (int aaa = m_dataViewListCtrl_Daily_Contacts->GetItemCount()-1; aaa >= 0; --aaa) {
			if (!m_dataViewListCtrl_Daily_Contacts->IsRowSelected(aaa)) {
				continue;
			}
			Contact contact = File_Manager::get_contact(contact_uuids[aaa]);
			Calendar_Manager::set_next_contact(contact, true, false, false);
			m_dataViewListCtrl_Daily_Contacts->DeleteItem(aaa);
			std::vector<string>::iterator it = contact_uuids.begin() + aaa;
			contact_uuids.erase(it);
		}
	} else {
		int row_id = m_dataViewListCtrl_Daily_Contacts->GetSelectedRow();
		Contact contact = File_Manager::get_contact(contact_uuids[row_id]);
		Calendar_Manager::set_next_contact(contact, true, false, false);
		m_dataViewListCtrl_Daily_Contacts->DeleteItem(row_id);
		std::vector<string>::iterator it = contact_uuids.begin() + row_id;
		contact_uuids.erase(it);
		/* auto select next contact, decided against to avoid accidentally marking a contact contacted due to the auto selection
		if (m_dataViewListCtrl_Daily_Contacts->GetItemCount() > row_id) {
			m_dataViewListCtrl_Daily_Contacts->SelectRow(row_id);
		} */
	}
	enable_buttons_check();
}

void wxPanel_Contact_Today::Reschedule( wxCommandEvent& event ) {
	if (m_dataViewListCtrl_Daily_Contacts->GetSelectedItemsCount() > 1) {
		for (int aaa = m_dataViewListCtrl_Daily_Contacts->GetItemCount()-1; aaa >= 0; --aaa) {
			if (!m_dataViewListCtrl_Daily_Contacts->IsRowSelected(aaa)) {
				continue;
			}
			Contact contact = File_Manager::get_contact(contact_uuids[aaa]);
			Calendar_Manager::reset_next_contact(contact, false, false, false);
			m_dataViewListCtrl_Daily_Contacts->DeleteItem(aaa);
			std::vector<string>::iterator it = contact_uuids.begin() + aaa;
			contact_uuids.erase(it);
		}
	} else {
		int row_id = m_dataViewListCtrl_Daily_Contacts->GetSelectedRow();
		Contact contact = File_Manager::get_contact(contact_uuids[row_id]);
		Calendar_Manager::reset_next_contact(contact, false, false, false);
		m_dataViewListCtrl_Daily_Contacts->DeleteItem(row_id);
		std::vector<string>::iterator it = contact_uuids.begin() + row_id;
		contact_uuids.erase(it);
		/* auto select next contact, decided against to avoid accidentally marking a contact rescheduled due to the auto selection
		if (m_dataViewListCtrl_Daily_Contacts->GetItemCount() > row_id) {
			m_dataViewListCtrl_Daily_Contacts->SelectRow(row_id);
		} */
	}
	enable_buttons_check();
}

void wxPanel_Contact_Today::Details( wxCommandEvent& event ) {
	int row_id = m_dataViewListCtrl_Daily_Contacts->GetSelectedRow();
	if (row_id == wxNOT_FOUND) {
		return;
	}
	string contact_uuid = contact_uuids[row_id];
	Contact contact = File_Manager::get_contact(contact_uuid);
	Window_Manager::Contact_Details(&contact, true);
}

void wxPanel_Contact_Today::New_Contact( wxCommandEvent& event ) {
	Contact contact;
	Window_Manager::Contact_Details(&contact, false);
}

void wxPanel_Contact_Today::All_Contacts( wxCommandEvent& event ) {
	Window_Manager::All_Contacts();
}

void wxPanel_Contact_Today::Social_Networks( wxCommandEvent& event ) {
	Window_Manager::Social_Networks();
}


void wxPanel_Contact_Today::Selection_Changes( wxCommandEvent& event ) {
	enable_buttons_check();
}

void wxPanel_Contact_Today::enable_buttons_check() {
	int selected_items_count = m_dataViewListCtrl_Daily_Contacts->GetSelectedItemsCount();
	m_button_Details->Enable(selected_items_count == 1);
	m_button_Contacted->Enable(selected_items_count > 0);
	m_button_Reschedule->Enable(selected_items_count > 0);
}
