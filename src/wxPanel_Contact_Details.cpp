/*
 * wxPanel_Contact_Details.cpp
 *
 *  Created on: Dec 21, 2023
 *      Author: oldma
 */

#include "wxPanel_Contact_Details.h"

#include <chrono>
#include <string>

#include "Calendar_Manager.h"
#include "File_Manager.h"
#include "Window_Manager.h"

wxPanel_Contact_Details::wxPanel_Contact_Details(wxWindow * parent, Contact * contact, bool update) :  wxPanel(parent, wxID_ANY)
{
	this->SetSizeHints( wxSize( 800,600 ), wxDefaultSize );

	wxGridBagSizer* gbSizer4;
	gbSizer4 = new wxGridBagSizer( 0, 0 );
	gbSizer4->SetFlexibleDirection( wxBOTH );
	gbSizer4->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	m_staticText3 = new wxStaticText( this, wxID_ANY, wxT("Name:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText3->Wrap( -1 );
	gbSizer4->Add( m_staticText3, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	m_staticText4 = new wxStaticText( this, wxID_ANY, wxT("First:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText4->Wrap( -1 );
	gbSizer4->Add( m_staticText4, wxGBPosition( 0, 1 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	m_staticText5 = new wxStaticText( this, wxID_ANY, wxT("Middle:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText5->Wrap( -1 );
	gbSizer4->Add( m_staticText5, wxGBPosition( 0, 3 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	m_staticText6 = new wxStaticText( this, wxID_ANY, wxT("Last:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText6->Wrap( -1 );
	gbSizer4->Add( m_staticText6, wxGBPosition( 0, 5 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	m_staticText7 = new wxStaticText( this, wxID_ANY, wxT("Username:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText7->Wrap( -1 );
	gbSizer4->Add( m_staticText7, wxGBPosition( 1, 1 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	m_staticText8 = new wxStaticText( this, wxID_ANY, wxT("Network:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText8->Wrap( -1 );
	gbSizer4->Add( m_staticText8, wxGBPosition( 2, 1 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	m_staticText10 = new wxStaticText( this, wxID_ANY, wxT("Frequency:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText10->Wrap( -1 );
	gbSizer4->Add( m_staticText10, wxGBPosition( 3, 3 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	m_staticText11 = new wxStaticText( this, wxID_ANY, wxT("Next Contact:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText11->Wrap( -1 );
	gbSizer4->Add( m_staticText11, wxGBPosition( 4, 1 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	m_staticText_Next_Contact = new wxStaticText( this, wxID_ANY, wxT("m_staticText_Next_Contact"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText_Next_Contact->Wrap( -1 );
	gbSizer4->Add( m_staticText_Next_Contact, wxGBPosition( 4, 2 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	m_staticText13 = new wxStaticText( this, wxID_ANY, wxT("Last Contact:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText13->Wrap( -1 );
	gbSizer4->Add( m_staticText13, wxGBPosition( 5, 1 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	m_staticText_Last_Contact = new wxStaticText( this, wxID_ANY, wxT("m_staticText_Last_Contact"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText_Last_Contact->Wrap( -1 );
	gbSizer4->Add( m_staticText_Last_Contact, wxGBPosition( 5, 2 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	m_staticText15 = new wxStaticText( this, wxID_ANY, wxT("Notes:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText15->Wrap( -1 );
	gbSizer4->Add( m_staticText15, wxGBPosition( 6, 0 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	m_textCtrl_First_Name = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	gbSizer4->Add( m_textCtrl_First_Name, wxGBPosition( 0, 2 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	m_textCtrl_Middle_Name = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	gbSizer4->Add( m_textCtrl_Middle_Name, wxGBPosition( 0, 4 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	m_textCtrl_Last_Name = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	gbSizer4->Add( m_textCtrl_Last_Name, wxGBPosition( 0, 6 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	m_textCtrl_Username = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	gbSizer4->Add( m_textCtrl_Username, wxGBPosition( 1, 2 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	wxArrayString m_choice_Social_NetworkChoices;
	m_choice_Social_Network = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_choice_Social_NetworkChoices, 0 );
	m_choice_Social_Network->SetSelection( 0 );
	gbSizer4->Add( m_choice_Social_Network, wxGBPosition( 2, 2 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	m_checkBox_Enabled = new wxCheckBox( this, wxID_ANY, wxT("Enabled:"), wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT );
	gbSizer4->Add( m_checkBox_Enabled, wxGBPosition( 3, 1 ), wxGBSpan( 1, 1 ), wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	wxArrayString m_choice_FrequencyChoices;
	m_choice_Frequency = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_choice_FrequencyChoices, 0 );
	m_choice_Frequency->SetSelection( 0 );
	gbSizer4->Add( m_choice_Frequency, wxGBPosition( 3, 4 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	m_textCtrl_Frequency = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	gbSizer4->Add( m_textCtrl_Frequency, wxGBPosition( 3, 6 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	m_button_Reschedule = new wxButton( this, wxID_ANY, wxT("Reschedule"), wxDefaultPosition, wxDefaultSize, 0 );
	gbSizer4->Add( m_button_Reschedule, wxGBPosition( 4, 3 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	m_button_Mark_Contacted = new wxButton( this, wxID_ANY, wxT("Mark Contacted"), wxDefaultPosition, wxDefaultSize, 0 );
	gbSizer4->Add( m_button_Mark_Contacted, wxGBPosition( 5, 3 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	m_textCtrl_Notes = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	m_textCtrl_Notes->SetMinSize( wxSize( 700,288 ) );

	gbSizer4->Add( m_textCtrl_Notes, wxGBPosition( 6, 1 ), wxGBSpan( 1, 6 ), wxALL, 5 );

	m_button_Save = new wxButton( this, wxID_ANY, wxT("Save"), wxDefaultPosition, wxDefaultSize, 0 );
	gbSizer4->Add( m_button_Save, wxGBPosition( 7, 1 ), wxGBSpan( 1, 1 ), wxALIGN_LEFT|wxALL, 5 );

	m_button_Delete = new wxButton( this, wxID_ANY, wxT("Delete"), wxDefaultPosition, wxDefaultSize, 0 );
	gbSizer4->Add( m_button_Delete, wxGBPosition( 7, 3 ), wxGBSpan( 1, 2 ), wxALIGN_CENTER_HORIZONTAL|wxALL, 5 );

	m_button_Back = new wxButton( this, wxID_ANY, wxT("Back"), wxDefaultPosition, wxDefaultSize, 0 );
	gbSizer4->Add( m_button_Back, wxGBPosition( 7, 6 ), wxGBSpan( 1, 1 ), wxALIGN_RIGHT|wxALL, 5 );


	this->SetSizer( gbSizer4 );
	this->Layout();

	this->Centre( wxBOTH );

	// Connect Events
	m_button_Reschedule->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_Contact_Details::Reschedule_Contact ), NULL, this );
	m_button_Mark_Contacted->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_Contact_Details::Mark_Contacted ), NULL, this );
	m_button_Save->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_Contact_Details::Save_Contact ), NULL, this );
	m_button_Delete->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_Contact_Details::Delete_Contact ), NULL, this );
	m_button_Back->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_Contact_Details::Back ), NULL, this );
	m_choice_Frequency->Connect( wxEVT_CHOICE, wxCommandEventHandler( wxPanel_Contact_Details::Frequency_Box_Update ), NULL, this );

	this->contact = *contact;
	this->update = update;
	build();
}

wxPanel_Contact_Details::~wxPanel_Contact_Details()
{
	// Disconnect Events
	m_button_Reschedule->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_Contact_Details::Reschedule_Contact ), NULL, this );
	m_button_Mark_Contacted->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_Contact_Details::Mark_Contacted ), NULL, this );
	m_button_Save->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_Contact_Details::Save_Contact ), NULL, this );
	m_button_Delete->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_Contact_Details::Delete_Contact ), NULL, this );
	m_button_Back->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_Contact_Details::Back ), NULL, this );
	m_choice_Frequency->Disconnect( wxEVT_CHOICE, wxCommandEventHandler( wxPanel_Contact_Details::Frequency_Box_Update ), NULL, this );
}

void wxPanel_Contact_Details::build() {
	std::vector<Social_Network> all_social_networks = File_Manager::get_all_social_networks();
	for (Social_Network social_network : all_social_networks) {
		social_network_nuids.push_back(social_network.get_nuid());
		m_choice_Social_Network->Append(social_network.network_name);
	}
	for (std::string frequency_string : Calendar_Manager::FREQUENCY_STRINGS) {
		m_choice_Frequency->Append(frequency_string);
	}
	m_button_Delete->Enable(update);

	load_contact();
}

void wxPanel_Contact_Details::load_contact() {
	using std::setw;
	using std::setfill;
	using std::chrono::year_month_day;
	m_textCtrl_First_Name->ChangeValue(contact.first_name);
	m_textCtrl_Middle_Name->ChangeValue(contact.middle_name);
	m_textCtrl_Last_Name->ChangeValue(contact.last_name);
	m_textCtrl_Username->ChangeValue(contact.username);
	std::stringstream date;
	if (contact.next_contact != year_month_day()) {
		date << setw(4) << setfill('0') << static_cast<int>(contact.next_contact.year()) << "-" <<
				setw(2) << setfill('0') << static_cast<unsigned>(contact.next_contact.month()) << "-" <<
				setw(2) << setfill('0') << static_cast<unsigned>(contact.next_contact.day());
	}
	m_staticText_Next_Contact->SetLabel(date.str());
	date.str("");
	if (contact.last_contact != year_month_day()) {
		date << setw(4) << setfill('0') << static_cast<int>(contact.last_contact.year()) << "-" <<
				setw(2) << setfill('0') << static_cast<unsigned>(contact.last_contact.month()) << "-" <<
				setw(2) << setfill('0') << static_cast<unsigned>(contact.last_contact.day());
	}
	m_staticText_Last_Contact->SetLabel(date.str());
	if (!update) {
		contact.enabled = true;
	}
	m_checkBox_Enabled->SetValue(contact.enabled);
	enabled_start_state = contact.enabled;
	if (contact.snid != "") {
		std::vector<std::string>::size_type index;
		for (index = 0; index < social_network_nuids.size(); index++) {
			if (social_network_nuids[index] == contact.snid) {
				break;
			}
		}
		m_choice_Social_Network->SetSelection(index);
	} else {
		// should only happen for brand new contact
		//Set first Social Network as default
		m_choice_Social_Network->SetSelection(0);
		contact.frequency = Calendar_Manager::MONTHLY;
	}
	if (contact.frequency <= 0) {
		m_choice_Frequency->SetSelection(0);
		m_textCtrl_Frequency->SetValue(std::to_string(-contact.frequency));
		m_textCtrl_Frequency->Show();
	} else {
		m_choice_Frequency->SetSelection(contact.frequency);
		m_textCtrl_Frequency->Hide();
	}
	m_textCtrl_Notes->SetValue(contact.notes);
}

void wxPanel_Contact_Details::update_contact() {
	contact.first_name = m_textCtrl_First_Name->GetValue();
	contact.middle_name = m_textCtrl_Middle_Name->GetValue();
	contact.last_name = m_textCtrl_Last_Name->GetValue();
	contact.username = m_textCtrl_Username->GetValue();
	std::string text = m_staticText_Last_Contact->GetLabel().ToStdString();
	if (text != "") {
		using std::chrono::day;
		using std::chrono::month;
		using std::chrono::year;
		using std::chrono::year_month_day;
		contact.last_contact = year_month_day(
				year(stoi(text.substr(0,4))),
				month(stoi(text.substr(5,2))),
				day(stoi(text.substr(8,2))));
	}
	contact.snid = social_network_nuids[m_choice_Social_Network->GetSelection()];
	contact.enabled = m_checkBox_Enabled->GetValue();
	contact.frequency = m_choice_Frequency->GetSelection();
	if (contact.frequency == 0) {
		contact.frequency = -stoi(m_textCtrl_Frequency->GetValue().ToStdString());
	}
	contact.notes = m_textCtrl_Notes->GetValue();
}

void wxPanel_Contact_Details::Reschedule_Contact( wxCommandEvent& event ) {
	wxMessageDialog reschedule_dialog_choice(this,
			"This will save any changes you have made to this Contact as part of rescheduling them. Have you completed all the changes you wanted to make?",
			"Save before Reschedule?",
			wxYES_NO | wxCANCEL);
	reschedule_dialog_choice.SetYesNoCancelLabels("Current Time Period",
			"Next Time Period",
			"Cancel Reschedule");
	int choice = reschedule_dialog_choice.ShowModal();
	if (choice == wxID_CANCEL) {
		return;
	}
	update_contact();
	if (contact.enabled) {
		if (choice == wxID_YES) {
			Calendar_Manager::reset_next_contact(contact, true, true, false);
		} else if (choice == wxID_NO) {
			Calendar_Manager::set_next_contact(contact, false, true, true);
		}
	} else {
		if (enabled_start_state != contact.enabled) {
			Calendar_Manager::remove_contact(contact);
			contact.next_contact = std::chrono::year_month_day();
		}
		File_Manager::update_contact(&contact);
	}
	Window_Manager::panel_back();
}

void wxPanel_Contact_Details::Mark_Contacted( wxCommandEvent& event ) {
	wxMessageDialog yes_no_dialog(this,
				"This will set today's date as the last contact date. Did you contact them today?",
				"Mark Contacted?",
				wxYES_NO);
	if (yes_no_dialog.ShowModal() != wxID_YES) {
		return;
	}
	using std::setw;
	using std::setfill;
	using std::to_string;
	std::stringstream date;
	std::chrono::year_month_day todays_date = Calendar_Manager::get_todays_date();
	date <<	setw(4) << setfill('0') << to_string(static_cast<int>(todays_date.year())) << "-" <<
			setw(2) << setfill('0') << to_string(static_cast<unsigned>(todays_date.month())) << "-" <<
			setw(2) << setfill('0') << to_string(static_cast<unsigned>(todays_date.day()));
	m_staticText_Last_Contact->SetLabel(date.str());
}

void wxPanel_Contact_Details::Save_Contact( wxCommandEvent& event ) {
	wxMessageDialog yes_no_dialog(this,
			"This will save any changes you have made to this Contact. Have you completed all the changes you wanted to make?",
			"Save before going back?",
			wxYES_NO);
	if (yes_no_dialog.ShowModal() != wxID_YES) {
		return;
	}
	update_contact();
	if (update) {
		if (enabled_start_state != contact.enabled) {
			if (contact.enabled) {
				Calendar_Manager::set_next_contact(contact, false, true, true);
			} else {
				Calendar_Manager::remove_contact(contact);
				contact.next_contact = std::chrono::year_month_day();
				File_Manager::update_contact(&contact);
			}
		} else {
			File_Manager::update_contact(&contact);
		}
	} else {
		contact.set_uuid();
		Calendar_Manager::set_next_contact(contact, false, true, true);
	}
	Window_Manager::panel_back();
}

void wxPanel_Contact_Details::Delete_Contact( wxCommandEvent& event ) {
	wxMessageDialog yes_no_dialog(this,
			"This will permanently delete this Contact. Are you sure you want to delete this contact?",
			"Delete Contact?",
			wxYES_NO);
	if (yes_no_dialog.ShowModal() != wxID_YES) {
		return;
	}
	if (update) {
		File_Manager::remove_contact(&contact);
		Calendar_Manager::remove_contact(contact);
	}
	Window_Manager::panel_back();
}

void wxPanel_Contact_Details::Back( wxCommandEvent& event ) {
	wxMessageDialog yes_no_dialog(this,
			"This will not save any changes you have made. Are you sure you want to go back?",
			"Go back without saving?",
			wxYES_NO);
	if (yes_no_dialog.ShowModal() != wxID_YES) {
		return;
	}
	Window_Manager::panel_back();
}

void wxPanel_Contact_Details::Frequency_Box_Update( wxCommandEvent& event ) {
	if (m_choice_Frequency->GetSelection() == Calendar_Manager::CUSTOM) {
		m_textCtrl_Frequency->Show();
	} else {
		m_textCtrl_Frequency->Hide();
	}
}
