/*
 * wxFrame_Social_Network_Edit.h
 *
 *  Created on: Jan 25, 2024
 *      Author: oldma
 */

#ifndef SRC_WXDIALOG_SOCIAL_NETWORK_EDIT_H_
#define SRC_WXDIALOG_SOCIAL_NETWORK_EDIT_H_

#include "wxObjects_includes.h"

class wxDialog_Social_Network_Edit : public wxDialog
{
	private:

	protected:
		wxStaticText* m_staticText15;
		wxStaticText* m_staticText16;
		wxButton* m_button_Okay;
		wxButton* m_button_Cancel;


	public:
		wxTextCtrl* m_textCtrl_Name;
		wxTextCtrl* m_textCtrl_URL;

		wxDialog_Social_Network_Edit(wxWindow * parent);

		~wxDialog_Social_Network_Edit();

};

#endif /* SRC_WXDIALOG_SOCIAL_NETWORK_EDIT_H_ */
