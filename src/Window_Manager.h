/*
 * Window_Manager.h
 *
 *  Created on: Nov 2, 2023
 *      Author: oldma
 */

#ifndef WINDOW_MANAGER_H_
#define WINDOW_MANAGER_H_

#include <string>
#include <vector>

#include "Contact.h"
#include "wxPanel_All_Contacts.h"

namespace Window_Manager{
	void main();
	void All_Contacts();
	void Contact_Details(Contact * contact, bool edit);
	void Contact_Today();
	void Social_Networks();
	void panel_back();
	void set_sort_option(wxPanel_All_Contacts::sort_enum so);
}



#endif /* WINDOW_MANAGER_H_ */
