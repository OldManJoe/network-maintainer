/*
 * File_Manager.h
 *
 *  Created on: Oct 12, 2022
 *      Author: Robert Higgins
 */

#ifndef FILE_MANAGER_H_
#define FILE_MANAGER_H_

#include <filesystem>
#include <regex>
#include <string>
#include <vector>

#include "Contact.h"
#include "Social_Network.h"

namespace File_Manager {
	enum command_enum {QUIT, GET_ALL, GET, EXISTS, GET_NETWORK_NAME, ADD, UPDATE, REMOVE, GREETINGS, QUESTIONS};
	enum command_argument_enum {CONTACT, SOCIAL_NETWORK};

	extern std::filesystem::path contacts_filename;
	extern std::filesystem::path social_networks_filename;

	std::string write_settings();
	void set_file_type(std::string line);

	// Contact commands
	std::vector<Contact> get_all_contacts(std::regex * re = NULL);
	Contact get_contact(std::string uuid);
	bool contact_exists(std::string uuid);
	bool add_contact(const Contact * contact, bool wait_for_return = false);
	bool update_contact(const Contact * contact, bool wait_for_return = false);
	bool remove_contact(const Contact * contact, bool wait_for_return = false);

	// Social_Network commands
	std::vector<Social_Network> get_all_social_networks(std::regex * re = NULL);
	Social_Network get_social_network(std::string nuid);
	bool social_network_exists(std::string nuid);
	std::string get_social_network_name(std::string nuid);
	bool add_social_network(const Social_Network * social_network, bool wait_for_return = false);
	bool update_social_network(const Social_Network * social_network, bool wait_for_return = false);
	bool remove_social_network(const Social_Network * social_network, bool wait_for_return = false);

	std::vector<std::string> get_greetings();
	std::vector<std::string> get_questions();

	void start();
	void join();
}

#endif /* FILE_MANAGER_H_ */
