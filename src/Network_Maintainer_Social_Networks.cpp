/*
 * Network_Maintainer_Social_Networks.cpp
 *
 *  Created on: August 5, 2023
 *      Author: Robert Higgins
 */

#include "Network_Maintainer.h"
#include "Network_Maintainer_Social_Networks.h"

#include <iostream>
#include <vector>

#include "File_Manager.h"
#include "Social_Network.h"

using std::cin;
using std::cout;
using std::endl;
using std::flush;
using std::string;
using std::stringstream;
using std::to_string;

namespace Network_Maintainer {
	Social_Network add_network(int order) {
		Social_Network new_social_network; string line;
		new_social_network.order = order;
		cout << "Social Network Name: "; getline(cin, line); new_social_network.network_name = line;
		cout << "Social Network URL: "; getline(cin, line); new_social_network.url = line;
		new_social_network.set_nuid();
		File_Manager::add_social_network(&new_social_network, false);
		cout << new_social_network.display() << endl;
		return new_social_network;
	}

	void edit_network(Social_Network & social_network) {
		string network_name, url;
		int choice = 0; char yesno;
		while (choice != 3) {
			cout << clear_screen(10);
			network_name = social_network.network_name; url = social_network.url;
			cout << "1. Network Name: " << network_name << "\n" <<
					"2. URL         : " << url << "\n" <<
					"3. Back" << "\n\n" <<
					"What do you want to edit? ";
			choice = 0; cin >> choice; flush_input();
			switch (choice) {
			case 1 : cout << "What is the new network name?" << endl; getline(cin, network_name); break;
			case 2 : cout << "What is the new url?" << endl; getline(cin, url); break;
			case 3 : File_Manager::update_social_network(&social_network); return;
			}
			if (choice > 0 && choice < 3) {
				cout << "Are you sure you want to make this change? [y]es [n]o: ";
				cin >> yesno; flush_input();
				if (yesno == 'y' || yesno == 'Y') {
					switch (choice) {
					case 1 : social_network.network_name = network_name; break;
					case 2 : social_network.url = url; break;
					}
				}
			}
		}
	}

	void remove_network(std::vector<Social_Network> & social_networks, Social_Network & social_network, int index) {
		cout << clear_screen(10) << social_network.display() << endl << endl <<
				"Are you sure you want to remove this social network? [y]es [n]o: ";
		char yesno = 'n'; cin >> yesno; flush_input(); std::vector<Social_Network>::iterator it;
		if (yesno == 'y' || yesno == 'Y') {
			File_Manager::remove_social_network(&social_network);
			it = social_networks.begin() + index;
			social_networks.erase(it);
		}
		it = social_networks.begin() + index;
		while (it != social_networks.end()) {
			--((*it).order);
			File_Manager::update_social_network(&*it);
			++it;
		}
	}

	void reorder_network(std::vector<Social_Network> & social_networks, Social_Network social_network, int dir) {
		int index = social_network.order - 1, swap_index = index + dir;
		if (swap_index < 0 || swap_index >= (long long)social_networks.size()) { return;
		}
		Social_Network swap(social_networks[swap_index]);
		social_network.order += dir;
		swap.order += -dir;
		social_networks[index] = swap;
		social_networks[swap_index] = social_network;
		File_Manager::update_social_network(&social_network);
		File_Manager::update_social_network(&swap);
	}

	void modify_social_network(std::vector<Social_Network> & social_networks, Social_Network & social_network, int index) {
		char letter = '\0';
		while (letter != 'b') {
			cout << "[E]dit     Move [U]p/[D]own     [R]emove     [B]ack" << endl
					<< "Enter letter for selection: ";
			cin >> letter; flush_input();
			switch (letter) {
			case 'e' : case 'E' : edit_network(social_network); return; break;
			case 'u' : case 'U' : reorder_network(social_networks, social_network, -1); return; break;
			case 'd' : case 'D' : reorder_network(social_networks, social_network, 1); return; break;
			case 'r' : case 'R' : remove_network(social_networks, social_network, index); return; break;
			case 'b' : case 'B' : return;
			default : break;
			}
		}
	}

	void modify_social_networks() {
		std::vector<Social_Network> social_networks = File_Manager::get_all_social_networks();
		int choice, count;
		while (true) {
			cout << clear_screen();
			count = 1;
			for (Social_Network social_network : social_networks) {
				cout << count << ". " << social_network.display() << endl;
				count++;
			}
			cout << count << ". Create New Social Network" << endl <<
					count + 1 << ". Back" << endl <<
					"Enter number for selection: ";
			cin >> choice; flush_input();
			if (choice > 0 && choice < count) { modify_social_network(social_networks, social_networks[choice - 1], choice - 1);
			} else if (choice == count) { social_networks.push_back(add_network(choice));
			} else if (choice == count + 1) { break;
			}
		}
	}

	string set_social_network_choice() {
		std::vector<Social_Network> social_networks = File_Manager::get_all_social_networks();
		int count, choice; string snid;
		do {
			count = 1;
			cout << endl << endl;
			for (Social_Network social_network : social_networks) {
				cout << count << ". " << social_network.display() << endl;
				count++;
			}
			cout << count << ". Create New Social Network" << endl <<
					"Enter number for selection: "; cin >> choice; flush_input();
			if (choice > 0 && choice < count) { snid = social_networks[choice - 1].get_nuid();
			} else if (choice == count) { social_networks.push_back(add_network(choice));
			}
		} while  (choice <= 0 || choice >= count);
		return snid;
	}
}
