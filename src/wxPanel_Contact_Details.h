/*
 * wxPanel_Contact_Details.h
 *
 *  Created on: Dec 21, 2023
 *      Author: oldma
 */

#ifndef WXPANEL_CONTACT_DETAILS_H_
#define WXPANEL_CONTACT_DETAILS_H_

#include "wxObjects_includes.h"

#include <string>
#include <vector>

#include "Contact.h"

class wxPanel_Contact_Details : public wxPanel
{
	private:
		Contact contact;
		bool update;
		bool edits_made;
		bool enabled_start_state;
		std::vector<std::string> social_network_nuids;

		void build();
		void update_contact();
		void load_contact();

	protected:
		wxStaticText* m_staticText3;
		wxStaticText* m_staticText4;
		wxStaticText* m_staticText5;
		wxStaticText* m_staticText6;
		wxStaticText* m_staticText7;
		wxStaticText* m_staticText8;
		wxStaticText* m_staticText10;
		wxStaticText* m_staticText11;
		wxStaticText* m_staticText13;
		wxStaticText* m_staticText15;
		wxButton* m_button_Reschedule;
		wxButton* m_button_Mark_Contacted;
		wxButton* m_button_Save;
		wxButton* m_button_Delete;
		wxButton* m_button_Back;
		wxTextCtrl* m_textCtrl_First_Name;
		wxTextCtrl* m_textCtrl_Middle_Name;
		wxTextCtrl* m_textCtrl_Last_Name;
		wxTextCtrl* m_textCtrl_Username;
		wxStaticText* m_staticText_Next_Contact;
		wxStaticText* m_staticText_Last_Contact;
		wxChoice* m_choice_Social_Network;
		wxCheckBox* m_checkBox_Enabled;
		wxChoice* m_choice_Frequency;
		wxTextCtrl* m_textCtrl_Frequency;
		wxTextCtrl* m_textCtrl_Notes;

		void Reschedule_Contact( wxCommandEvent& event );
		void Mark_Contacted( wxCommandEvent& event );
		void Save_Contact( wxCommandEvent& event );
		void Delete_Contact( wxCommandEvent& event );
		void Back( wxCommandEvent& event );
		void Frequency_Box_Update( wxCommandEvent& event );

	public:

		wxPanel_Contact_Details(wxWindow * parent, Contact * contact, bool update);

		~wxPanel_Contact_Details();
};

#endif /* WXPANEL_CONTACT_DETAILS_H_ */
