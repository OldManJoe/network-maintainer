/*
 * File_Manager.cpp
 *
 *  Created on: Oct 12, 2022
 *      Author: Robert Higgins
 */

#include "File_Manager.h"

#include <condition_variable>
#include <filesystem>
#include <fstream>
#include <memory>
#include <mutex>
#include <queue>
#include <regex>
#include <vector>

#include "Contact.h"
#include "CSV_File_Manager.h"
#include "Global_Variables.h"
#include "Logging.h"
#include "Network_Maintainer.h"
#include "Social_Network.h"

using std::endl;
using std::string;
using std::stringstream;
using std::to_string;

namespace File_Manager {
	// public variables
	std::filesystem::path contacts_filename("../data/contacts");
	std::filesystem::path social_networks_filename("../data/social_networks");

	// private enums, constants, structs, and classes
	enum file_type_enum {CSV, XML, SQL};

	struct Command {
		command_enum command;
		command_argument_enum object_type;
		string str;
		Contact contact;
		Social_Network social_network;
		std::regex * re;
		void * reply;
		std::mutex * reply_mutex;
	};

	// private variables
	file_type_enum data_file_type;

	// thead control
	std::thread file_manager_thread;
	std::queue<Command> command_queue;
	std::mutex queue_mutex;
	std::condition_variable not_empty;

	// helper methods
	void check_files() {
		switch (data_file_type) {
		case CSV: CSV_File_Manager::check_files(); break;
		default:
			Logging::log("FILE_EXCEPTION: data_file_type not implemented or incorrect, recommend delete settings file and relaunching program");
			throw FILE_EXCEPTION;
			break;
		}
	}

	// public methods
	string write_settings() {
		stringstream settings; settings << "file_type ";
		switch (data_file_type) {
		case CSV : settings << "csv " << CSV_File_Manager::csv_separator << endl; break;
		default : Logging::log("UNKNOWN_ERROR: data_file_type got corrupted, restart program"); throw UNKNOWN_ERROR; break;
		}
		return settings.str();
	}

	void set_file_type(string line) {
		stringstream line_reader(line);
		string file_type; line_reader >> file_type;
		if (file_type == "csv") {
			File_Manager::data_file_type = CSV;
			line_reader.get(); // get and discard the space between csv and the deliminating character
			CSV_File_Manager::csv_separator = line_reader.get();
			stringstream log_output; log_output << "File_Manager: File type CSV, separator: " << CSV_File_Manager::csv_separator;
			Logging::log(log_output.str());
		} else { Logging::log("FILE_EXCEPTION: set_file_type setting malformed, recommend delete settings file and relaunching program"); throw FILE_EXCEPTION;
		}
	}

	// Commands
	Command get_command() {
		Logging::log("File_Manager::get_command()");
		std::unique_lock<std::mutex> lock(queue_mutex);
		not_empty.wait(lock, [](){return !command_queue.empty();});
		Command current_command = command_queue.front(); command_queue.pop();
		return current_command;
	}

	void send_command(Command command) {
		std::unique_lock<std::mutex> lock(queue_mutex);
		command_queue.push(command);
		not_empty.notify_one();
	}

	void _quit() {
		Logging::log("File_Manager::_quit()");
		throw NORMAL_EXIT;
	}

	void _get_all(Command command) {
		Logging::log("File_Manager::_get_all(Command command)");
		switch (data_file_type) {
		case CSV :
			switch(command.object_type) {
			case CONTACT: *(std::vector<Contact> *)command.reply = CSV_File_Manager::get_all_contacts(command.re); break;
			case SOCIAL_NETWORK: *(std::vector<Social_Network> *)command.reply = CSV_File_Manager::get_all_social_networks(command.re); break;
			default : Logging::log("BROKEN_COMMAND: File_Manager get_all command malformed, contact programmers to correct problem"); throw BROKEN_COMMAND; break;
			}
			break;
		default : Logging::log("UNKNOWN_ERROR: data_file_type got corrupted, restart program"); throw UNKNOWN_ERROR; break;
		}
		command.reply_mutex->unlock();
	}

	void get_all(command_argument_enum object_type, std::regex * re, void * reply) {
		Command c;
		c.command = GET_ALL;
		c.object_type = object_type;
		c.re = re;
		c.reply = reply;
		std::mutex reply_mutex; c.reply_mutex = &reply_mutex;
		reply_mutex.lock();
		send_command(c);
		reply_mutex.lock();
	}

	std::vector<Contact> get_all_contacts(std::regex * re) {
		Logging::log("File_Manager::get_all_contacts(std::regex * re)");
		std::vector<Contact> reply;
		get_all(CONTACT, re, &reply);
		return reply;
	}

	std::vector<Social_Network> get_all_social_networks(std::regex * re) {
		Logging::log("File_Manager::get_all_social_networks(std::regex * re)");
		std::vector<Social_Network> reply;
		get_all(SOCIAL_NETWORK, re, &reply);
		return reply;
	}

	void _get_exists(Command command) {
		Logging::log("File_Manager::_get_exists(Command command)");
		switch (data_file_type) {
		case CSV :
			switch (command.command) {
			case GET :
				switch (command.object_type) {
				case CONTACT : *(Contact *)command.reply = CSV_File_Manager::get_contact(command.str); break;
				case SOCIAL_NETWORK : *(Social_Network *)command.reply = CSV_File_Manager::get_social_network(command.str); break;
				}
				break;
			case EXISTS : *(bool *)command.reply = CSV_File_Manager::exists(command.object_type, command.str); break;
			default :
				Logging::log("BROKEN_COMMAND: File_Manager get_exists command malformed, contact programmers to correct problem");
				throw BROKEN_COMMAND;
				break;
			}
			break;
		default : Logging::log("UNKNOWN_ERROR: data_file_type got corrupted, restart program"); throw UNKNOWN_ERROR; break;
		}

		command.reply_mutex->unlock();
	}

	void get_exists(command_enum command, command_argument_enum object_type, string id, void * reply) {
		Command c;
		c.command = command;
		c.object_type = object_type;
		c.str = id;
		c.reply = reply;
		std::mutex reply_mutex; c.reply_mutex = &reply_mutex;
		reply_mutex.lock();
		send_command(c);
		reply_mutex.lock();
	}

	Contact get_contact(string uuid) {
		Logging::log("File_Manager::get_contact(string uuid)");
		Contact reply;
		get_exists(GET, CONTACT, uuid, &reply);
		return reply;
	}

	Social_Network get_social_network(string nuid) {
		Logging::log("File_Manager::get_social_network(string nuid)");
		Social_Network reply;
		get_exists(GET, SOCIAL_NETWORK, nuid, &reply);
		return reply;
	}

	bool contact_exists(string uuid) {
		Logging::log("File_Manager::contact_exists(string uuid)");
		bool reply;
		get_exists(EXISTS, CONTACT, uuid, &reply);
		return reply;
	}

	bool social_network_exists(string nuid) {
		Logging::log("File_Manager::social_network_exists(string nuid)");
		bool reply;
		get_exists(EXISTS, SOCIAL_NETWORK, nuid, &reply);
		return reply;
	}

	void _get_social_network_name(Command command) {
		Logging::log("File_Manager::_get_social_network_name(Command command)");
		switch (data_file_type) {
		case CSV : *(string *)command.reply = CSV_File_Manager::get_social_network_name(command.str); break;
		default : Logging::log("UNKNOWN_ERROR: data_file_type got corrupted, restart program"); throw UNKNOWN_ERROR; break;
		}
		command.reply_mutex->unlock();
	}

	string get_social_network_name(string nuid) {
		Logging::log("File_Manager::get_social_network_name(string nuid)");
		Command c;
		c.command = GET_NETWORK_NAME;
		c.str = nuid;
		string reply; c.reply = &reply;
		std::mutex reply_mutex; c.reply_mutex = &reply_mutex;
		reply_mutex.lock();
		send_command(c);
		reply_mutex.lock();
		return reply;
	}

	void _add_update_remove(Command command) {
		Logging::log("File_Manager::_add_update_remove(Command command)");
		void * object;
		switch (command.object_type) {
		case CONTACT : object = &command.contact; break;
		case SOCIAL_NETWORK : object = &command.social_network; break;
		default : Logging::log("BROKEN_COMMAND: File_Manager add_update_remove command malformed, contact programmers to correct problem"); throw BROKEN_COMMAND; break;
		}
		bool response = false;
		switch (data_file_type) {
		case CSV : response = CSV_File_Manager::add_update_remove(command.command, command.object_type, object); break;
		default : Logging::log("UNKNOWN_ERROR: data_file_type got corrupted, restart program"); throw UNKNOWN_ERROR; break;
		}
		if (command.reply != NULL) { *(bool *)command.reply = response;
		}
		if (command.reply_mutex != NULL) { command.reply_mutex->unlock();
		}
	}

	void add_update_remove(command_enum command, command_argument_enum object_type, const void * object, bool wait_for_return, bool * reply) {
		Command c;
		c.command = command;
		c.object_type = object_type;
		switch (object_type) {
		case CONTACT : c.contact = *(Contact *)object; break;
		case SOCIAL_NETWORK : c.social_network = *(Social_Network *)object; break;
		default : Logging::log("BROKEN_COMMAND: File_Manager add_update_remove command malformed, contact programmers to correct problem"); throw BROKEN_COMMAND; break;
		}
		c.reply = (wait_for_return ? reply : NULL);
		std::mutex reply_mutex; c.reply_mutex = (wait_for_return ? &reply_mutex : NULL);
		if (wait_for_return) { reply_mutex.lock();
		}
		send_command(c);
		if (wait_for_return) { reply_mutex.lock();
		}
	}

	bool add_contact(const Contact * contact, bool wait_for_return) {
		Logging::log("File_Manager::add_contact(const Contact * contact, bool wait_for_return)");
		bool reply = false;
		add_update_remove(ADD, CONTACT, contact, wait_for_return, &reply);
		return reply;
	}

	bool update_contact(const Contact * contact, bool wait_for_return) {
		Logging::log("File_Manager::update_contact(const Contact * contact, bool wait_for_return)");
		bool reply = false;
		add_update_remove(UPDATE, CONTACT, contact, wait_for_return, &reply);
		return reply;
	}

	bool remove_contact(const Contact * contact, bool wait_for_return) {
		Logging::log("File_Manager::remove_contact(const Contact * contact, bool wait_for_return)");
		bool reply = false;
		add_update_remove(REMOVE, CONTACT, contact, wait_for_return, &reply);
		return reply;
	}

	bool add_social_network(const Social_Network * social_network, bool wait_for_return) {
		Logging::log("File_Manager::add_social_network(const Social_Network * social_network, bool wait_for_return)");
		bool reply = false;
		add_update_remove(ADD, SOCIAL_NETWORK, social_network, wait_for_return, &reply);
		return reply;
	}

	bool update_social_network(const Social_Network * social_network, bool wait_for_return) {
		Logging::log("File_Manager::update_social_network(const Social_Network * social_network, bool wait_for_return)");
		bool reply = false;
		add_update_remove(UPDATE, SOCIAL_NETWORK, social_network, wait_for_return, &reply);
		return reply;
	}

	bool remove_social_network(const Social_Network * social_network, bool wait_for_return) {
		Logging::log("File_Manager::remove_social_network(const Social_Network * social_network, bool wait_for_return)");
		bool reply = false;
		add_update_remove(REMOVE, SOCIAL_NETWORK, social_network, wait_for_return, &reply);
		return reply;
	}

	void _get_prompt(Command command) {
		std::filesystem::path prompt_list_filename;
		if (command.command == QUESTIONS) { prompt_list_filename = "../prompts/questions.txt";
		} else if (command.command == GREETINGS) { prompt_list_filename = "../prompts/greetings.txt";
		}
		std::ifstream prompt_list_file(prompt_list_filename); string line;
		while (prompt_list_file) {
			getline(prompt_list_file, line);
			if (line != "") { ((std::vector<string> *)command.reply)->push_back(line);
			}
		}
		command.reply_mutex->unlock();
	}

	std::vector<string> get_prompt(command_enum prompt_type) {
		Logging::log("File_Manager::get_prompt(command_enum prompt_type)");
		Command c;
		c.command = prompt_type;
		std::vector<string> reply; c.reply = &reply;
		std::mutex reply_mutex; c.reply_mutex = &reply_mutex;
		reply_mutex.lock();
		send_command(c);
		reply_mutex.lock();
		return reply;
	}

	std::vector<string> get_greetings() {
		Logging::log("File_Manager::get_greetings()");
		return get_prompt(GREETINGS);
	}

	std::vector<string> get_questions() {
		Logging::log("File_Manager::get_questions()");
		return get_prompt(QUESTIONS);
	}

	// thread control
	void loop() {
		while (true) {
			Command current_command = get_command();
			switch (current_command.command) {
			case QUIT : _quit(); break;
			case GET_ALL : _get_all(current_command);break;
			case GET : case EXISTS : _get_exists(current_command); break;
			case GET_NETWORK_NAME : _get_social_network_name(current_command); break;
			case ADD : case UPDATE : case REMOVE : _add_update_remove(current_command); break;
			case GREETINGS : case QUESTIONS : _get_prompt(current_command); break;
			default : break;
			}
		}
	}

	void main() {
		try {
			check_files();
			loop();
		} catch (const exception_flag_enum & exception_flag) {
			if (exception_flag != NORMAL_EXIT) { Network_Maintainer::throw_flag(exception_flag);
			}
			return;
		} catch (const std::exception & e) {
			Network_Maintainer::throw_exception(e);
			Logging::log(e.what());
			return;
		}
	}

	void start() {
		file_manager_thread = std::thread(main);
	}

	void join() {
		Logging::log("File_Manager::join()");
		Command c; c.command = QUIT; File_Manager::send_command(c);
		if (file_manager_thread.joinable()) { file_manager_thread.join();
		}
	}
}
