/*
 * Logging.cpp
 *
 *  Created on: Mar 6, 2023
 *      Author: Robert Higgins
 */

#include "Logging.h"

#include <algorithm>
#include <chrono>
#include <condition_variable>
#include <filesystem>
#include <fstream>
#include <mutex>
#include <queue>
#include <sstream>
#include <string>

#include "Global_Variables.h"
#include "Network_Maintainer.h"

#define DEFAULT_LOG_COUNT 5

using std::endl;
using std::string;
using std::to_string;

namespace Logging {
	// public variables

	// private enums, constatns, structs, and classes
	const string QUIT_STRING = "Logging::quit();";
	int log_count = DEFAULT_LOG_COUNT;

	// private variables
	std::filesystem::path log_file;

	//thread control
	std::thread logging_thread;
	std::queue<string> log_queue;
	std::mutex queue_mutex;
	std::condition_variable not_empty;

	// helper methods
	void write(string line) {
		std::ofstream file;
		file.open(log_file, std::ios::app);
		file << line << endl;
	}

	void remove_old_logs(std::filesystem::path log_directory) {
		std::filesystem::directory_iterator dir_it(log_directory);
		std::vector<std::filesystem::path> paths;
		int count = 0;
		for (const std::filesystem::directory_entry & entry : dir_it) {
		    if (entry.is_regular_file()) { count++; paths.push_back(entry.path());
		    }
		}
		if (count >= log_count) {
			std::sort(paths.begin(), paths.end());
			for (int aaa = 0; aaa <= (count - log_count); aaa++) { std::filesystem::remove(paths[aaa]);
			}
		}
	}

	void start_log() {
		using std::chrono::days;
		using std::chrono::system_clock;
		system_clock::time_point now = std::chrono::system_clock::now();
		std::chrono::time_point<system_clock, days> now_floor = std::chrono::floor<days>(now);
		std::chrono::year_month_day today(std::chrono::floor<days>(now));
		std::chrono::hh_mm_ss<system_clock::duration> time((now - now_floor));
		std::stringstream filename;
		filename << "../logs/log_file_" << static_cast<int>(today.year()) <<
				std::setw(2) << std::setfill('0') << static_cast<unsigned>(today.month()) <<
				std::setw(2) << std::setfill('0') << static_cast<unsigned>(today.day()) << "-" <<
				std::setw(2) << std::setfill('0') << (time.hours().count() % 24) <<
				std::setw(2) << std::setfill('0') << time.minutes().count() <<
				std::setw(2) << std::setfill('0') << time.seconds().count() <<
				".log";
		log_file = std::filesystem::path(filename.str());
		std::filesystem::path log_directory = log_file;
		log_directory.remove_filename();
		if (std::filesystem::is_directory(log_directory)) { remove_old_logs(log_directory);
		} else { std::filesystem::create_directory(log_directory);
		}
		std::ofstream file_write(log_file);
	}

	// public methods
	string write_settings() {
		std::stringstream settings;
		if (log_count != DEFAULT_LOG_COUNT) { settings << "log_count " << log_count << endl;
		}
		return settings.str();
	}

	void set_log_count(int count) {
		if (count < 0) { return;
		}
		log_count = count;
	}

	// Commands
	string get_log() {
		std::unique_lock<std::mutex> lock(queue_mutex);
		not_empty.wait(lock, [](){return !log_queue.empty();});
		string current_print = log_queue.front(); log_queue.pop();
		return current_print;
	}

	void log(string line) {
		std::unique_lock<std::mutex> lock(queue_mutex);
		log_queue.push(line);
		not_empty.notify_one();
	}

	// thread control
	void loop() {
		while (true) {
			string current_log = get_log();
			write(current_log);
			if (current_log == QUIT_STRING) { throw NORMAL_EXIT;
			}
		}
	}

	void main() {
		try {
			start_log();
			loop();
		} catch (const exception_flag_enum & exception_flag) {
			if (exception_flag != NORMAL_EXIT) { Network_Maintainer::throw_flag(exception_flag);
			}
			return;
		} catch (const std::exception & e) {
			Network_Maintainer::throw_exception(e);
			write(e.what());
			return;
		}
	}

	void start() {
		logging_thread = std::thread(main);
	}

	void join() {
		Logging::log("Logging::join()");
		log(QUIT_STRING);
		if (logging_thread.joinable()) { logging_thread.join();
		}
	}
}

