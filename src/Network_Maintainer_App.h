/*
 * NetworkMaintainerApp.h
 *
 *  Created on: Dec 28, 2023
 *      Author: oldma
 */

#ifndef SRC_NETWORK_MAINTAINER_APP_H_
#define SRC_NETWORK_MAINTAINER_APP_H_

// For compilers that support precompilation, includes "wx/wx.h".
#include <wx/wxprec.h>

#ifndef WX_PRECOMP
    #include <wx/wx.h>
#endif

class Network_Maintainer_App : public wxApp {
public:
    virtual bool OnInit();
    int ExitInstance();
};

enum
{
    ID_Hello = 1
};

#endif /* SRC_NETWORK_MAINTAINER_APP_H_ */
