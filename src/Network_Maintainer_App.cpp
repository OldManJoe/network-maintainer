/*
 * NetworkMaintainerApp.cpp
 *
 *  Created on: Dec 28, 2023
 *      Author: oldma
 */

#include "Network_Maintainer_App.h"

#include <iostream>
#include "Window_Manager.h"

wxIMPLEMENT_APP_NO_MAIN(Network_Maintainer_App);

bool Network_Maintainer_App::OnInit() {
	Window_Manager::main();

    return true;
}

int Network_Maintainer_App::ExitInstance() {
   // OnExit isn't called by CleanUp so must be called explicitly.
   wxTheApp->OnExit();
   wxApp::CleanUp();

   std::cout << "ExitInstance called" << std::endl;

   //return CWinApp::ExitInstance();
   return 0;
}
