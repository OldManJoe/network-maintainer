/*
 * Social_Network.h
 *
 *  Created on: Jan 30, 2023
 *      Author: Robert Higgins
 */

#ifndef SOCIAL_NETWORK_H_
#define SOCIAL_NETWORK_H_

#include <string>

class Social_Network {
private:
	std::string nuid; // Network UID

public :
	enum members_enum {NUID, NETWORK_NAME, URL, ORDER};
	static const int MEMBER_NUM = ORDER+1;

	int order;
	std::string network_name;
	std::string url;
	// std::string loginID;
	// LoginMethod (optional)
	// bool enabled;

	Social_Network();
	void set_nuid();
	void set_nuid(std::string nuid);
	std::string get_nuid() const;
	std::string display() const;

};



#endif /* SOCIAL_NETWORK_H_ */
