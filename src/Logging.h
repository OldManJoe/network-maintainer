/*
 * Logging.h
 *
 *  Created on: Mar 6, 2023
 *      Author: Robert Higgins
 */

#ifndef LOGGING_H_
#define LOGGING_H_

#include <string>

namespace Logging {
	std::string write_settings();
	void set_log_count(int count);

	void log(std::string line);
	void start();
	void join();
}

#endif /* LOGGING_H_ */
