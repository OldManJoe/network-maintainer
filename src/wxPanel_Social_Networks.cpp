/*
 * wxPanel_Social_Networks.cpp
 *
 *  Created on: Dec 21, 2023
 *      Author: oldma
 */

#include "wxPanel_Social_Networks.h"

#include <string>

#include "File_Manager.h"
#include "wxDialog_Social_Network_Edit.h"
#include "Window_Manager.h"

wxPanel_Social_Networks::wxPanel_Social_Networks(wxWindow * parent) :  wxPanel(parent, wxID_ANY)
{
	this->SetSizeHints( wxSize( 800,600 ), wxDefaultSize );

	wxGridBagSizer* gbSizer2;
	gbSizer2 = new wxGridBagSizer( 0, 0 );
	gbSizer2->SetFlexibleDirection( wxBOTH );
	gbSizer2->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	m_button_Edit_Social_Network = new wxButton( this, wxID_ANY, wxT("Edit"), wxDefaultPosition, wxDefaultSize, 0 );
	gbSizer2->Add( m_button_Edit_Social_Network, wxGBPosition( 0, 1 ), wxGBSpan( 1, 1 ), wxALIGN_CENTER|wxALL, 5 );
	m_button_Edit_Social_Network->Enable(false);

	m_button_Copy_Social_Network = new wxButton( this, wxID_ANY, wxT("Copy"), wxDefaultPosition, wxDefaultSize, 0 );
	gbSizer2->Add( m_button_Copy_Social_Network, wxGBPosition( 0, 3 ), wxGBSpan( 1, 1 ), wxALIGN_CENTER|wxALL, 5 );
	m_button_Copy_Social_Network->Enable(false);

	m_button_Add_Social_Network = new wxButton( this, wxID_ANY, wxT("Add"), wxDefaultPosition, wxDefaultSize, 0 );
	gbSizer2->Add( m_button_Add_Social_Network, wxGBPosition( 0, 5 ), wxGBSpan( 1, 1 ), wxALIGN_CENTER|wxALL, 5 );

	m_button_Delete_Social_Network = new wxButton( this, wxID_ANY, wxT("Delete"), wxDefaultPosition, wxDefaultSize, 0 );
	gbSizer2->Add( m_button_Delete_Social_Network, wxGBPosition( 0, 7 ), wxGBSpan( 1, 1 ), wxALIGN_CENTER|wxALL, 5 );
	m_button_Delete_Social_Network->Enable(false);

	m_dataViewListCtrl_Social_Networks = new wxDataViewListCtrl( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0 );
	m_dataViewListCtrl_Social_Networks->SetMinSize( wxSize( 775,400 ) );

	m_dataViewListColumn5 = m_dataViewListCtrl_Social_Networks->AppendTextColumn( wxT("Name"), wxDATAVIEW_CELL_INERT, 385, static_cast<wxAlignment>(wxALIGN_LEFT), wxDATAVIEW_COL_RESIZABLE );
	m_dataViewListColumn6 = m_dataViewListCtrl_Social_Networks->AppendTextColumn( wxT("URL"), wxDATAVIEW_CELL_INERT, 385, static_cast<wxAlignment>(wxALIGN_LEFT), wxDATAVIEW_COL_RESIZABLE );
	gbSizer2->Add( m_dataViewListCtrl_Social_Networks, wxGBPosition( 1, 0 ), wxGBSpan( 1, 9 ), wxALL, 5 );

	m_button_Move_Down = new wxButton( this, wxID_ANY, wxT("Move Down"), wxDefaultPosition, wxDefaultSize, 0 );
	gbSizer2->Add( m_button_Move_Down, wxGBPosition( 2, 1 ), wxGBSpan( 1, 1 ), wxALL, 5 );
	m_button_Move_Down->Enable(false);

	m_button_Move_Up = new wxButton( this, wxID_ANY, wxT("Move Up"), wxDefaultPosition, wxDefaultSize, 0 );
	gbSizer2->Add( m_button_Move_Up, wxGBPosition( 2, 3 ), wxGBSpan( 1, 1 ), wxALL, 5 );
	m_button_Move_Up->Enable(false);

	m_button_Back = new wxButton( this, wxID_ANY, wxT("Back"), wxDefaultPosition, wxDefaultSize, 0 );
	gbSizer2->Add( m_button_Back, wxGBPosition( 2, 7 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	this->SetSizer( gbSizer2 );
	this->Layout();

	this->Centre( wxBOTH );

	// Connect Events
	m_button_Edit_Social_Network->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_Social_Networks::Edit ), NULL, this );
	m_button_Copy_Social_Network->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_Social_Networks::Copy_Social_Network ), NULL, this );
	m_button_Add_Social_Network->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_Social_Networks::Add_Social_Network ), NULL, this );
	m_button_Delete_Social_Network->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_Social_Networks::Delete_Social_Network ), NULL, this );
	m_button_Move_Down->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_Social_Networks::Move_Down ), NULL, this );
	m_button_Move_Up->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_Social_Networks::Move_Up ), NULL, this );
	m_button_Back->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_Social_Networks::Back ), NULL, this );
	m_dataViewListCtrl_Social_Networks->Connect( wxEVT_DATAVIEW_SELECTION_CHANGED, wxCommandEventHandler( wxPanel_Social_Networks::Selection_Changes ), NULL, this );

	build();
}

wxPanel_Social_Networks::~wxPanel_Social_Networks()
{
	// Disconnect Events
	m_button_Edit_Social_Network->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_Social_Networks::Edit ), NULL, this );
	m_button_Copy_Social_Network->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_Social_Networks::Copy_Social_Network ), NULL, this );
	m_button_Add_Social_Network->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_Social_Networks::Add_Social_Network ), NULL, this );
	m_button_Delete_Social_Network->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_Social_Networks::Delete_Social_Network ), NULL, this );
	m_button_Move_Down->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_Social_Networks::Move_Down ), NULL, this );
	m_button_Move_Up->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_Social_Networks::Move_Up ), NULL, this );
	m_button_Back->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_Social_Networks::Back ), NULL, this );
	m_dataViewListCtrl_Social_Networks->Disconnect( wxEVT_DATAVIEW_SELECTION_CHANGED, wxCommandEventHandler( wxPanel_Social_Networks::Selection_Changes ), NULL, this );
}

void wxPanel_Social_Networks::build() {
	std::vector<Social_Network> all_social_networks = File_Manager::get_all_social_networks();
	wxVector<wxVariant> data;
	for (Social_Network social_network : all_social_networks) {
		data.clear();
		data.push_back(social_network.network_name);
		data.push_back(social_network.url);
		social_network_nuids.push_back(social_network.get_nuid());
		m_dataViewListCtrl_Social_Networks->AppendItem(data);
	}
}

void wxPanel_Social_Networks::Edit( wxCommandEvent& event ) {
	int row_id = m_dataViewListCtrl_Social_Networks->GetSelectedRow();
	if (row_id == wxNOT_FOUND) {
		return;
	}
	wxDialog_Social_Network_Edit edit_dialog(this);
	std::string social_network_nuid = social_network_nuids[row_id];
	Social_Network edit_network = File_Manager::get_social_network(social_network_nuid);
	edit_dialog.m_textCtrl_Name->SetValue(edit_network.network_name);
	edit_dialog.m_textCtrl_URL->SetValue(edit_network.url);
	if (edit_dialog.ShowModal() != wxID_OK) {
		return;
	}
	edit_network.network_name = edit_dialog.m_textCtrl_Name->GetValue();
	edit_network.url = edit_dialog.m_textCtrl_URL->GetValue();
	File_Manager::update_social_network(&edit_network);
	m_dataViewListCtrl_Social_Networks->SetValue(edit_network.network_name, row_id, 0);
	m_dataViewListCtrl_Social_Networks->SetValue(edit_network.url, row_id, 1);
}
void wxPanel_Social_Networks::Copy_Social_Network( wxCommandEvent& event ) {
	int row_id = m_dataViewListCtrl_Social_Networks->GetSelectedRow();
	if (row_id == wxNOT_FOUND) {
		return;
	}
	wxDialog_Social_Network_Edit copy_dialog(this);
	std::string social_network_nuid = social_network_nuids[row_id];
	Social_Network copy_network = File_Manager::get_social_network(social_network_nuid);
	copy_dialog.m_textCtrl_Name->SetValue(copy_network.network_name);
	copy_dialog.m_textCtrl_URL->SetValue(copy_network.url);
	if (copy_dialog.ShowModal() != wxID_OK) {
		return;
	}
	Social_Network new_social_network;
	new_social_network.network_name = copy_dialog.m_textCtrl_Name->GetValue();
	new_social_network.url = copy_dialog.m_textCtrl_URL->GetValue();
	new_social_network.order = social_network_nuids.size() + 1;
	new_social_network.set_nuid();
	File_Manager::add_social_network(&new_social_network);
	wxVector<wxVariant> data;
	data.push_back(new_social_network.network_name);
	data.push_back(new_social_network.url);
	m_dataViewListCtrl_Social_Networks->AppendItem(data);
	social_network_nuids.push_back(new_social_network.get_nuid());
}
void wxPanel_Social_Networks::Add_Social_Network( wxCommandEvent& event ) {
	wxDialog_Social_Network_Edit add_dialog(this);
	if (add_dialog.ShowModal() != wxID_OK) {
		return;
	}
	Social_Network new_social_network;
	new_social_network.network_name = add_dialog.m_textCtrl_Name->GetValue();
	new_social_network.url = add_dialog.m_textCtrl_URL->GetValue();
	new_social_network.order = social_network_nuids.size() + 1;
	new_social_network.set_nuid();
	File_Manager::add_social_network(&new_social_network);
	wxVector<wxVariant> data;
	data.push_back(new_social_network.network_name);
	data.push_back(new_social_network.url);
	m_dataViewListCtrl_Social_Networks->AppendItem(data);
	social_network_nuids.push_back(new_social_network.get_nuid());
}
void wxPanel_Social_Networks::Delete_Social_Network( wxCommandEvent& event ) {
	int row_id = m_dataViewListCtrl_Social_Networks->GetSelectedRow();
	if (row_id == wxNOT_FOUND) {
		return;
	}
	std::string social_network_nuid = social_network_nuids[row_id];
	std::vector<Social_Network> all_social_networks = File_Manager::get_all_social_networks();
	File_Manager::remove_social_network(&all_social_networks[row_id]);
	for (unsigned aaa = row_id + 1; aaa < all_social_networks.size(); ++aaa) {
		--all_social_networks[aaa].order;
		File_Manager::update_social_network(&all_social_networks[aaa]);
	}
	m_dataViewListCtrl_Social_Networks->DeleteItem(row_id);
	std::vector<std::string>::iterator it = social_network_nuids.begin() + row_id;
	social_network_nuids.erase(it);

	enable_buttons_check();
}
void wxPanel_Social_Networks::Move_Social_Network(int dir) {
	int row_id = m_dataViewListCtrl_Social_Networks->GetSelectedRow();
	if (row_id == wxNOT_FOUND) {
		return;
	}
	int swap_id = row_id + dir;
	if (swap_id < 0 || swap_id >= (long long)social_network_nuids.size()) {
		return;
	}
	std::string social_network_nuid = social_network_nuids[row_id];
	std::string swap_nuid = social_network_nuids[swap_id];
	Social_Network social_network = File_Manager::get_social_network(social_network_nuid);
	Social_Network swap_network = File_Manager::get_social_network(swap_nuid);
	social_network.order += dir;
	swap_network.order += -dir;
	m_dataViewListCtrl_Social_Networks->SetValue(swap_network.network_name, row_id, 0);
	m_dataViewListCtrl_Social_Networks->SetValue(swap_network.url, row_id, 1);
	m_dataViewListCtrl_Social_Networks->SetValue(social_network.network_name, swap_id, 0);
	m_dataViewListCtrl_Social_Networks->SetValue(social_network.url, swap_id, 1);
	m_dataViewListCtrl_Social_Networks->SelectRow(swap_id);
	social_network_nuids[row_id] = swap_nuid;
	social_network_nuids[swap_id] = social_network_nuid;
	File_Manager::update_social_network(&social_network);
	File_Manager::update_social_network(&swap_network);
}
void wxPanel_Social_Networks::Move_Down( wxCommandEvent& event ) {
	Move_Social_Network(1);
}
void wxPanel_Social_Networks::Move_Up( wxCommandEvent& event ) {
	Move_Social_Network(-1);
}
void wxPanel_Social_Networks::Back( wxCommandEvent& event ) {
	Window_Manager::panel_back();
}

void wxPanel_Social_Networks::Selection_Changes( wxCommandEvent& event ) {
	enable_buttons_check();
}

void wxPanel_Social_Networks::enable_buttons_check() {
	bool enable_buttons = m_dataViewListCtrl_Social_Networks->GetSelectedItemsCount() == 1;
	m_button_Edit_Social_Network->Enable(enable_buttons);
	m_button_Copy_Social_Network->Enable(enable_buttons);
	m_button_Move_Down->Enable(enable_buttons);
	m_button_Move_Up->Enable(enable_buttons);
	m_button_Delete_Social_Network->Enable(enable_buttons);
}

