/*
 * File_Manager.cpp
 *
 *  Created on: Oct 12, 2022
 *      Author: Robert Higgins
 */

#include "CSV_File_Manager.h"

#include <algorithm>
#include <filesystem>
#include <fstream>
#include <iomanip>
#include <regex>
#include <sstream>
#include <string>
#include <vector>

#include "Contact.h"
#include "File_Manager.h"
#include "Global_Variables.h"
#include "Logging.h"
#include "Social_Network.h"

#define CONTACT_HEADER_LINE "uuid;next_contact;enabled;first_name;middle_name;last_name;username;snid;contact_frequency;last_contact;notes"
#define TEST_CONTACT_LINE "12345678901234567890;12345678;0;Test;;User;Test_Username;12345678901234567890;-1;20230101;This user is a test user and should never be enabled or displayed"
#define SOCIAL_NETWORK_HEADER_LINE "nuid;network_name;url;order"
#define TEST_SOCIAL_NETWORK_LINE "12345678901234567890;Test Network;https://www.test_network.com;0"

using std::endl;
using std::string;
using std::stringstream;
using std::to_string;
using File_Manager::command_argument_enum;
using File_Manager::CONTACT;
using File_Manager::SOCIAL_NETWORK;
using File_Manager::contacts_filename;
using File_Manager::social_networks_filename;

namespace CSV_File_Manager {
	// public variables
	char csv_separator;

	// private enums, constants, structs, and classes

	// methods
	string get_line_from_csv_file(std::filesystem::path file, string line_identifier) {
		std::ifstream file_stream(file); string line;
		while (file_stream) {
			getline(file_stream, line);
			if (line.contains(line_identifier)) { return line;
			}
		}
		return "";
	}

	string contact_to_csv_line(const Contact & contact) {
		stringstream csv_line;
		csv_line << contact.get_uuid() << csv_separator <<
				std::setw(4) << std::setfill('0') << to_string(static_cast<int>(contact.next_contact.year())) <<
				std::setw(2) << std::setfill('0') << to_string(static_cast<unsigned>(contact.next_contact.month())) <<
				std::setw(2) << std::setfill('0') << to_string(static_cast<unsigned>(contact.next_contact.day())) << csv_separator <<
				to_string(contact.enabled) << csv_separator <<
				contact.first_name << csv_separator <<
				contact.middle_name << csv_separator <<
				contact.last_name << csv_separator <<
				contact.username << csv_separator <<
				contact.snid << csv_separator <<
				to_string(contact.frequency) << csv_separator <<
				std::setw(4) << std::setfill('0') << to_string(static_cast<int>(contact.last_contact.year())) <<
				std::setw(2) << std::setfill('0') << to_string(static_cast<unsigned>(contact.last_contact.month())) <<
				std::setw(2) << std::setfill('0') << to_string(static_cast<unsigned>(contact.last_contact.day())) << csv_separator <<
				contact.notes;
		return csv_line.str();
	}

	Contact csv_line_to_contact(string line) {
		using std::chrono::day;
		using std::chrono::month;
		using std::chrono::year;
		using std::chrono::year_month_day;
		string text[Contact::MEMBER_NUM];
		string::size_type prev_index = 0, next_index = -1;
		for (int aaa = 0; aaa < Contact::MEMBER_NUM; aaa++) {
			prev_index = next_index+1; next_index = line.find(csv_separator, prev_index);
			text[aaa] = line.substr(prev_index, next_index - prev_index);
		}
		Contact c;
		c.set_uuid(text[Contact::UUID]);
		c.next_contact = year_month_day(
				year(stoi(text[Contact::NEXT_CONTACT].substr(0,4))),
				month(stoi(text[Contact::NEXT_CONTACT].substr(4,2))),
				day(stoi(text[Contact::NEXT_CONTACT].substr(6,2))));
		c.enabled = stoi(text[Contact::ENABLED]);
		c.first_name = text[Contact::FIRST_NAME];
		c.middle_name = text[Contact::MIDDLE_NAME];
		c.last_name = text[Contact::LAST_NAME];
		c.username = text[Contact::USERNAME];
		c.snid = text[Contact::SNID];
		c.frequency = stoi(text[Contact::FREQUENCY]);
		c.last_contact = year_month_day(
				year(stoi(text[Contact::LAST_CONTACT].substr(0,4))),
				month(stoi(text[Contact::LAST_CONTACT].substr(4,2))),
				day(stoi(text[Contact::LAST_CONTACT].substr(6,2))));
		c.notes = text[Contact::NOTES];
		return c;
	}

	string social_network_to_csv_line(const Social_Network & social_network) {
		stringstream csv_line;
		csv_line << social_network.get_nuid() << csv_separator <<
				social_network.network_name << csv_separator <<
				social_network.url << csv_separator <<
				social_network.order;
		return csv_line.str();
	}

	Social_Network csv_line_to_social_network(string line) {
		string text[Social_Network::MEMBER_NUM];
		string::size_type prev_index = 0;
		string::size_type next_index = -1;
		for (int aaa = 0; aaa < Social_Network::MEMBER_NUM; aaa++) {
			prev_index = next_index+1;
			next_index = line.find(csv_separator, prev_index);
			text[aaa] = line.substr(prev_index, next_index - prev_index);
		}
		Social_Network sn;
		sn.set_nuid(text[Social_Network::NUID]); // Network UID
		sn.network_name = text[Social_Network::NETWORK_NAME];
		sn.url = text[Social_Network::URL];
		sn.order = stoi(text[Social_Network::ORDER]);
		return sn;
	}

	void check_file(std::filesystem::path file, string default_lines) {
		std::ifstream file_open; file_open.open(file);
		if (!file_open) {
			std::filesystem::path directory = file; directory.remove_filename();
			if (!std::filesystem::is_directory(directory)) {
				std::filesystem::create_directory(directory);
			}
			std::ofstream file_write(file);
			if (file_write) {
				file_write << default_lines;
			} else {
				Logging::log("FILE_EXCEPTION: could not create " + file.string());
				throw FILE_EXCEPTION;
			}
		}
	}

	void check_files() {
		std::filesystem::path extension(".csv");
		stringstream default_lines; default_lines << CONTACT_HEADER_LINE << endl << TEST_CONTACT_LINE << endl;
		contacts_filename.replace_extension(extension);
		check_file(contacts_filename, default_lines.str());
		default_lines.str(string()); default_lines << SOCIAL_NETWORK_HEADER_LINE << endl << TEST_SOCIAL_NETWORK_LINE << endl;
		social_networks_filename.replace_extension(extension);
		check_file(social_networks_filename, default_lines.str());
	}

	std::vector<Contact> get_all_contacts(std::regex * re) {
		std::vector<Contact> contacts; std::ifstream contacts_file(contacts_filename);
		Contact contact; string line;
		while (contacts_file) {
			getline(contacts_file, line);
			if (line == "" || line == CONTACT_HEADER_LINE || line == TEST_CONTACT_LINE) {
				continue;
			}
			contact = csv_line_to_contact(line);
			stringstream search_string;
			search_string << contact.first_name << " " << contact.middle_name << " " << contact.last_name << " " << contact.username;
			if (re == NULL || std::regex_search(search_string.str(), *re)) {
				contacts.push_back(contact);
			}
		}
		return contacts;
	}

	Contact get_contact(string uuid) {
		string line = get_line_from_csv_file(contacts_filename, uuid);
		if (line == "") {
			Logging::log("UID_NOT_FOUND: get_contact(string uuid = " + uuid + ")");
			throw UID_NOT_FOUND;
		}
		return csv_line_to_contact(line);
	}

	std::vector<Social_Network> get_all_social_networks(std::regex * re) {
		std::ifstream social_networks_file(social_networks_filename);
		std::vector<Social_Network> social_networks; Social_Network sn;
		string line;
		while (social_networks_file.good()) {
			getline(social_networks_file, line);
			if (line == "" || line == SOCIAL_NETWORK_HEADER_LINE || line == TEST_SOCIAL_NETWORK_LINE) {
				continue;
			}
			if (re == NULL || std::regex_search(line, *re)) {
				sn = csv_line_to_social_network(line);
				social_networks.push_back(sn);
			}
		}
		std::sort(social_networks.begin(), social_networks.end(),
				[](Social_Network a, Social_Network b){return a.order < b.order;});
		return social_networks;
	}

	Social_Network get_social_network(string nuid) {
		string line = get_line_from_csv_file(social_networks_filename, nuid);
		if (line == "") {
			Logging::log("UID_NOT_FOUND: get_social_network(string nuid = " + nuid + ")");
			throw UID_NOT_FOUND;
		}
		return csv_line_to_social_network(line);
	}

	bool exists(command_argument_enum object_type, std::string id){
		string line; line = get_line_from_csv_file(contacts_filename, id);
		switch (object_type) {
		case CONTACT :
			line = get_line_from_csv_file(contacts_filename, id); break;
		case SOCIAL_NETWORK :
			line = get_line_from_csv_file(social_networks_filename, id); break;
		}
		return line != "";
	}

	string get_social_network_name(string nuid) {
		string line = get_line_from_csv_file(social_networks_filename, nuid);
		if (line == "") {
			return "";
		}
		string::size_type first_index = line.find(csv_separator, 0) + 1;
		string::size_type second_index = line.find(csv_separator, first_index);
		return line.substr(first_index, second_index - first_index);
	}

	bool _add(std::filesystem::path file, string line) {
		if (DEBUG) {
			stringstream debug_output;
			debug_output << "CSV_File_Manager::_add(std::filesystem::path file = " << file << ", string line = " << line << ")";
			Logging::log(debug_output.str());
		}
		std::ofstream file_write(file, std::ios::app);
		if (file_write) {
			file_write << line << endl;
			return true;
		} else {
			return false;
		}
	}

	bool add(command_argument_enum object_type, void * object) {
		if (DEBUG) {
			stringstream debug_output;
			debug_output << "CSV_File_Manager::add(command_argument_enum object_type = " << object_type <<
					", void * object = " << object << ")";
			Logging::log(debug_output.str());
		}
		switch (object_type) {
		case CONTACT :
			return _add(contacts_filename, contact_to_csv_line(*(Contact *)object)); break;
		case SOCIAL_NETWORK :
			return _add(social_networks_filename, social_network_to_csv_line(*(Social_Network *)object)); break;
		default :
			Logging::log("BROKEN_COMMAND: CSV_File_Manager add command malformed, contact programmers to correct problem");
			throw BROKEN_COMMAND; break;
		}
	}

	bool _remove(std::filesystem::path file, string line_identifier) {
		if (DEBUG) {
			stringstream debug_output;
			debug_output << "CSV_File_Manager::_remove(std::filesystem::path file = " << file << ", string line_identifier = " <<
					line_identifier << ")";
			Logging::log(debug_output.str());
		}
		if (line_identifier == "") {
			return false;
		}
		std::ifstream file_open(file);
		std::filesystem::path file_temp = file;
		file_temp += ".tmp";
		std::ofstream file_write(file_temp);
		string line;
		if (!file_open || !file_write) {
			return false;
		}
		while (file_open) {
			getline(file_open, line);
			if (line != "" && !line.contains(line_identifier)) {
				file_write << line << endl;
			}
		}
		file_open.close();
		file_write.close();
		std::filesystem::rename(file_temp, file);
		return true;
	}

	bool remove(command_argument_enum object_type, void * object) {
		if (DEBUG) {
			stringstream debug_output;
			debug_output << "CSV_File_Manager::remove(command_argument_enum object_type = " << object_type <<
					", void * object = " << object << ")";
			Logging::log(debug_output.str());
		}
		switch (object_type) {
		case CONTACT :
			return _remove(contacts_filename, ((Contact *)object)->get_uuid()); break;
		case SOCIAL_NETWORK :
			return _remove(social_networks_filename, ((Social_Network *)object)->get_nuid()); break;
		default :
			Logging::log("BROKEN_COMMAND: CSV_File_Manager remove command malformed, contact programmers to correct problem"); throw BROKEN_COMMAND; break;
		}
	}

	bool _update(std::filesystem::path file, string line, string line_identifier) {
		if (DEBUG) {
			stringstream debug_output;
			debug_output << "CSV_File_Manager::_update(std::filesystem::path file = " << file << ", string line = " <<
					line << " string line_identifier = " << line_identifier << ")";
			Logging::log(debug_output.str());
		}
		bool response = _remove(file, line_identifier);
		if (!response) {
			return false;
		}
		response = _add(file, line); return response;
	}

	bool update(command_argument_enum object_type, void * object) {
		if (DEBUG) {
			stringstream debug_output;
			debug_output << "CSV_File_Manager::update(command_argument_enum object_type = " << object_type <<
					", void * object = " << object << ")";
			Logging::log(debug_output.str());
		}
		switch (object_type) {
		case CONTACT :
			return _update(contacts_filename, contact_to_csv_line(*(Contact *)object), ((Contact *)object)->get_uuid()); break;
		case SOCIAL_NETWORK :
			return _update(social_networks_filename, social_network_to_csv_line(*(Social_Network *)object), ((Social_Network *)object)->get_nuid()); break;
		default :
			Logging::log("BROKEN_COMMAND: CSV_File_Manager update command malformed, contact programmers to correct problem");
			throw BROKEN_COMMAND; break;
		}
	}

	bool add_update_remove(File_Manager::command_enum command, command_argument_enum object_type, void * object) {
		switch (command) {
		case File_Manager::ADD :
			return add(object_type, object); break;
		case File_Manager::UPDATE :
			return update(object_type, object); break;
		case File_Manager::REMOVE :
			return remove(object_type, object); break;
		default :
			Logging::log("BROKEN_COMMAND: CSV_File_Manager add_update_remove command malformed, contact programmers to correct problem");
			throw BROKEN_COMMAND; break;
		}
	}
}
