/*
 * Window_Manager.cpp
 *
 *  Created on: Nov 2, 2023
 *      Author: oldma
 */

#include <wx/wxprec.h>

#ifndef WX_PRECOMP
    #include <wx/wx.h>
#endif

#include <string>
#include <vector>

#include "Global_Variables.h"

#include "Window_Manager.h"
#include "wxPanel_Contact_Today.h"
#include "wxPanel_Social_Networks.h"
#include "wxPanel_All_Contacts.h"
#include "wxPanel_Contact_Details.h"

namespace Window_Manager {
	wxFrame * main_frame;

	enum panel_id {CONTACT_TODAY, SOCIAL_NETWORKS, ALL_CONTACTS, CONTACT_DETAILS};
	panel_id active_panel_id;
	wxPanel * active_panel;
	std::vector<panel_id> panel_stack;
	wxPanel_All_Contacts::sort_enum sort_option = wxPanel_All_Contacts::LAST_NAME;

	void change_panel(panel_id panel_id, Contact * contact = NULL, bool update = false) {
		wxPanel * old_panel = active_panel;
		active_panel_id = panel_id;
		switch (panel_id) {
		case CONTACT_TODAY :
			active_panel = new wxPanel_Contact_Today(main_frame); break;
		case SOCIAL_NETWORKS :
			active_panel = new wxPanel_Social_Networks(main_frame); break;
		case ALL_CONTACTS :
			active_panel = new wxPanel_All_Contacts(main_frame, sort_option); break;
		case CONTACT_DETAILS :
			active_panel = new wxPanel_Contact_Details(main_frame, contact, update); break;
		default :
			throw BROKEN_COMMAND; break;
		}
		old_panel->Destroy();
		active_panel->Show(true);
		main_frame->Layout();
	}

	void All_Contacts() {
		panel_stack.push_back(active_panel_id);
		change_panel(ALL_CONTACTS);
	}
	void Contact_Details(Contact * contact, bool update) {
		panel_stack.push_back(active_panel_id);
		change_panel(CONTACT_DETAILS, contact, update);
	}
	void Contact_Today() {
		panel_stack.push_back(active_panel_id);
		change_panel(CONTACT_TODAY);
	}
	void Social_Networks() {
		panel_stack.push_back(active_panel_id);
		change_panel(SOCIAL_NETWORKS);
	}

	void panel_back() {
		if (panel_stack.back() == CONTACT_DETAILS) {
			panel_stack.pop_back();
			panel_back();
		}
		change_panel(panel_stack.back());
		panel_stack.pop_back();
	}

	void set_sort_option(wxPanel_All_Contacts::sort_enum so) {
		sort_option = so;
	}

	void build_main_frame() {
		main_frame = new wxFrame(nullptr, wxID_ANY, "Network Maintainer", wxDefaultPosition, wxSize(800,600));
//		main_frame->SetMaxSize(wxSize(800,600));
		main_frame->SetMinSize(wxSize(800,600));
		main_frame->Center();
		main_frame->Show(true);
	}

	void main() {
		build_main_frame();
		active_panel = new wxPanel_Contact_Today(main_frame);
		active_panel_id = CONTACT_TODAY;
		active_panel->Show(true);
		main_frame->Layout();
	}

}
