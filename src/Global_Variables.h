/*
 * Global_Variables.h
 *
 *  Created on: Mar 4, 2023
 *      Author: Robert Higgins
 */

#ifndef GLOBAL_VARIABLES_H_
#define GLOBAL_VARIABLES_H_

#include <random>

enum exception_flag_enum {NORMAL_EXIT, FILE_EXCEPTION, BROKEN_COMMAND, UID_NOT_FOUND, UNKNOWN_ERROR};

extern bool DEBUG;
extern std::mt19937_64 mt;

#endif /* GLOBAL_VARIABLES_H_ */
