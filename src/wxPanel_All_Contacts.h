/*
 * wxPanel_All_Contacts.h
 *
 *  Created on: Dec 21, 2023
 *      Author: oldma
 */

#ifndef WXPANEL_ALL_CONTACTS_H_
#define WXPANEL_ALL_CONTACTS_H_

#include "wxObjects_includes.h"

#include <string>
#include <vector>

#include "Contact.h"

class wxPanel_All_Contacts : public wxPanel
{
	public:
		enum sort_enum {FIRST_NAME, LAST_NAME, USERNAME, LAST_CONTACT, NEXT_CONTACT};

		wxDataViewListCtrl* m_dataViewListCtrl_Contacts;

		wxPanel_All_Contacts(wxWindow * parent, sort_enum sort_option);

		~wxPanel_All_Contacts();

	protected:
		wxStaticText* m_staticText2;
		wxTextCtrl* m_textCtrl_Find_Contact;
		wxButton* m_button_Find_Contact;
		wxButton* m_button_Details;
		wxButton* m_button_New_Contact;
		wxButton* m_button_Copy_Contact;
		wxButton* m_button_Delete_Contact;
		wxDataViewColumn* m_dataViewListColumn7;
		wxDataViewColumn* m_dataViewListColumn8;
		wxDataViewColumn* m_dataViewListColumn9;
		wxButton* m_button_Reschedule_All;
		wxStaticText* m_staticText3;
		wxChoice* m_choice_Sort_Option;
		wxButton* m_button_Back;

		void Find_Contact( wxCommandEvent& event );
		void Contact_Details( wxCommandEvent& event );
		void New_Contact( wxCommandEvent& event );
		void Copy_Contact( wxCommandEvent& event );
		void Delete_Contact( wxCommandEvent& event );
		void Reschedule_All_Contacts( wxCommandEvent& event );
		void Sort( wxCommandEvent& event );
		void Back( wxCommandEvent& event );
		void Selection_Changes( wxCommandEvent& event );

		void enable_buttons_check();

	private:
		std::vector<std::string> contact_uuids;

		void build(sort_enum sort_option);
		void load_contacts(std::vector<Contact> all_contacts);
};

#endif /* WXPANEL_ALL_CONTACTS_H_ */
