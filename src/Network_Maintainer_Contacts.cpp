/*
 * Network_Maintainer_Contacts.cpp
 *
 *  Created on: August 5, 2023
 *      Author: Robert Higgins
 */

#include "Network_Maintainer.h"
#include "Network_Maintainer_Contacts.h"
#include "Network_Maintainer_Social_Networks.h"

#include <array>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "Calendar_Manager.h"
#include "Contact.h"
#include "File_Manager.h"

#define CONTACT_LIST_HEADER "first_name;last_name"
#define CONTACT_LIST_EXAMPLE "Test;User"

using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::stringstream;
using std::to_string;

namespace Network_Maintainer {
	enum sort_enum {FIRST_NAME, LAST_NAME, USERNAME, LAST_CONTACT, NEXT_CONTACT};
	const int SORT_OPTIONS_NUM = NEXT_CONTACT+1;
	const std::array<string, SORT_OPTIONS_NUM> SORT_STRINGS = {"By first name", "By last name", "By username", "By last contact", "By next contact"};

	string trim_whitespace(const string &s) {
		string s2 = std::regex_replace(s, std::regex("^\\s+"), string(""));
		return std::regex_replace(s2, std::regex("\\s+$"), string(""));
	}

	void force_reschedule_all_contacts() {
		cout << clear_screen();
		cout << "Are you sure you want to reschedule all contacts for their next contact period?" << endl <<
				"This cannot be undone. Please type CONFIRM: ";
		string confirm; getline(cin, confirm);
		if (confirm != "CONFIRM") {
			cout << "Incorrect confirmation" << endl;
			return;
		}
		cout << "Setting next contacts for all contacts" << endl;
		std::vector<Contact> all_contacts = File_Manager::get_all_contacts();
		for (Contact contact : all_contacts) {
			if(contact.enabled) {
				Calendar_Manager::set_next_contact(contact, false, true, false);
			}
		}
	}

	int set_frequency_choice() {
		int choice, frequency;
		do {
			cout << endl << endl;
			cout << "Contact Frequency: " << endl;
			for (int aaa = 1; aaa < Calendar_Manager::FREQUENCY_NUM; aaa++) {
				cout << aaa << ". " << Calendar_Manager::FREQUENCY_STRINGS_OPTIONS[aaa] << endl;
			}
			cout << Calendar_Manager::FREQUENCY_NUM << ". " << Calendar_Manager::FREQUENCY_STRINGS_OPTIONS[Calendar_Manager::CUSTOM] << endl <<
					"Enter number for selection: ";
			cin >> choice; flush_input(); frequency = choice;
			if (choice == Calendar_Manager::FREQUENCY_NUM) {
				do {
					cout << endl << endl;
					cout << "You selected a custom number of days, how many days?" << endl;
					cout << "Select -1 for back" << endl;
					cout << "Enter number for selection: ";
					cin >> choice;
					if (!cin) {
						choice = -2;
					}
					flush_input();
				} while (choice < -1);
				if (choice != -1) {
					frequency = -choice;
					choice = Calendar_Manager::FREQUENCY_NUM;
				}
			}
		} while (choice <= 0 || choice > Calendar_Manager::FREQUENCY_NUM);
		return frequency;
	}

	void add_person() {
		Contact new_contact; string line;
		cout << clear_screen() << "Adding new person to contact list: " << endl << endl <<
				"First Name: "; getline(cin, line); new_contact.first_name = trim_whitespace(line);
		cout << "Middle Name: "; getline(cin, line); new_contact.middle_name = trim_whitespace(line);
		cout << "Last Name: "; getline(cin, line); new_contact.last_name = trim_whitespace(line);
		cout << endl << endl << "Social Network Username if applicable: "; getline(cin, line); new_contact.username = trim_whitespace(line);
		new_contact.frequency = set_frequency_choice();
		new_contact.snid = set_social_network_choice();
		new_contact.enabled = true;
		new_contact.set_uuid();
		Calendar_Manager::set_next_contact(new_contact, false, false, true);
		cout << endl << endl << new_contact.display_short() << endl << endl << endl <<
				"Do you want to add another contact? [y/n] ";
		char yesno = 'n'; cin >> yesno; flush_input();
		if (yesno == 'y' || yesno == 'Y') {
			return add_person();
		}
	}

	void add_persons_from_list() {
		string contact_list_filename_string;
		cout << clear_screen();
		cout << "To add contacts from a list, make sure the list is in the format: " << endl <<
				"first_name; last_name" << endl <<
				"You must use a semicolon (;)" << endl <<
				"Are you ready to read in a file? [y]es/[n]o: ";
		char yesno = 'n'; cin >> yesno; flush_input();
		if (yesno != 'y' && yesno != 'Y') {
			return;
		}
		cout << "Enter the absolute path to the contact list you want to read in: ";
		cout.flush(); getline(cin, contact_list_filename_string);
		std::filesystem::path contact_list_filename(contact_list_filename_string);
		std::ifstream contact_list_file(contact_list_filename);
		if (!contact_list_file) {
			cout << "Failed to open file" << endl; return;
		} else {
			cout << "Opened file : " << contact_list_filename << endl;
		}
		int frequency = set_frequency_choice();
		string snid = set_social_network_choice();

		while (contact_list_file) {
			int contact_list_num = 2;
			string line, text[contact_list_num];
			string::size_type prev_index = 0, next_index = -1;
			getline(contact_list_file, line);
			if (line == "" || line == CONTACT_LIST_HEADER || line == CONTACT_LIST_EXAMPLE) {
				continue;
			}
			for (int aaa = 0; aaa < contact_list_num; aaa++) {
				prev_index = next_index+1; next_index = line.find(';', prev_index);
				text[aaa] = line.substr(prev_index, next_index - prev_index);
			}
			Contact new_contact;
			new_contact.first_name = trim_whitespace(text[0]);
			new_contact.last_name = trim_whitespace(text[1]);
			new_contact.frequency = frequency;
			new_contact.snid = snid;
			new_contact.enabled = true;
			new_contact.set_uuid();
			Calendar_Manager::set_next_contact(new_contact, false, false, true);
		}
	}

	void reschedule_person(Contact & contact) {
		char letter = '\0';
		while (letter != 'b') {
			cout << "Reschedule next contact within [c]urrent time frame or [n]ext time frame, [d]o not reschedule" << endl <<
					"Enter letter for selection: "; cin >> letter; flush_input();
			switch (letter) {
			case 'c' : case 'C' :
				Calendar_Manager::reset_next_contact(contact, true, true, false);
				return; break;
			case 'n' : case 'N' :
				cout << "Do you want to set today as last contact? [y]es/[n]o? "; cin >> letter; flush_input();
				Calendar_Manager::set_next_contact(contact, letter == 'y' || letter == 'Y', true, true);
				return; break;
			case 'd' : case 'D' :
				return; break;
			default :
				break;
			}
		}
	}

	void edit_person(Contact & contact) {
		string frequency_str, line, input_str, enabled_str, social_network;
		bool enabled_start_state;
		int choice = 0, input_int = 0; char yesno;
		if (contact.frequency < 1) {
			frequency_str = Calendar_Manager::FREQUENCY_STRINGS_OPTIONS[0] + ": " + to_string(-contact.frequency);
		} else {
			frequency_str = Calendar_Manager::FREQUENCY_STRINGS[contact.frequency];
		}
		social_network = File_Manager::get_social_network_name(contact.snid);
		enabled_str = contact.enabled ? "True" : "False";
		enabled_start_state = contact.enabled;
		while (choice != 9) {
			cout << clear_screen(10);
			cout << "1. First Name    : " << contact.first_name << "\n" <<
					"2. Middle Name   : " << contact.middle_name << "\n" <<
					"3. Last Name     : " << contact.last_name << "\n" <<
					"4. Username      : " << contact.username << "\n" <<
					"5. Frequency     : " << frequency_str << "\n" <<
					"6. Social Network: " << social_network << "\n" <<
					"7. Enabled       : " << enabled_str << "\n" <<
					"8. Notes         : " << contact.notes << "\n" <<
					"9. Back" << "\n\n" <<
					"What do you want to edit? ";
			choice = 0; cin >> choice; flush_input();
			switch (choice) {
			case 1 : cout << "What is the new first name? "; getline(cin, input_str); input_str = trim_whitespace(input_str); break;
			case 2 : cout << "What is the new middle name? "; getline(cin, input_str); input_str = trim_whitespace(input_str); break;
			case 3 : cout << "What is the new last name? "; getline(cin, input_str); input_str = trim_whitespace(input_str); break;
			case 4 : cout << "What is the new username? "; getline(cin, input_str); input_str = trim_whitespace(input_str); break;
			case 5 : input_int = set_frequency_choice(); break;
			case 6 : input_str = set_social_network_choice(); break;
			case 7 : enabled_str = !contact.enabled ? "True" : "False"; break;
			case 8 : cout << "Notes will be replaced with what you enter here, copy and paste any notes you want to keep\n" <<
							 "Notes: "; getline(cin, line); input_str = line; break;
			case 9 :
				File_Manager::update_contact(&contact);
				if (enabled_start_state != contact.enabled) {
					if (contact.enabled) {
						Calendar_Manager::set_next_contact(contact, false, true, true);
					} else {
						Calendar_Manager::remove_contact(contact);
					}
				}
				return; break;
			default : break;
			} // end switch (choice)
			if (choice > 0 && choice < 9) {
				cout << "Are you sure you want to make this change? [y]es/[n]o: ";
				cin >> yesno; flush_input();
				if (yesno == 'y' || yesno == 'Y') {
					switch (choice) {
					case 1 : contact.first_name = input_str; break;
					case 2 : contact.middle_name = input_str; break;
					case 3 : contact.last_name = input_str; break;
					case 4 : contact.username = input_str; break;
					case 5 :
						contact.frequency = input_int;
						if (contact.frequency < 1) { frequency_str = Calendar_Manager::FREQUENCY_STRINGS_OPTIONS[0] + ": " + to_string(-contact.frequency);
						} else { frequency_str = Calendar_Manager::FREQUENCY_STRINGS[input_int];
						}
						reschedule_person(contact);
						break;
					case 6 : contact.snid = input_str; social_network = File_Manager::get_social_network_name(contact.snid); break;
					case 7 : contact.enabled = !contact.enabled; break;
					case 8 : contact.notes = input_str; break;
					default : break;
					}
				}
			} // end if (choice > 0 && choice < 9)
		} // end while
	}

	void remove_person(std::vector<Contact> & contacts, Contact & contact, int index) {
		cout << clear_screen(10) << contact.display() << "\n\n" <<
				"Are you sure you want to remove this person? [y]es [n]o: ";
		char yesno = 'n'; cin >> yesno; flush_input();
		if (yesno == 'y' || yesno == 'Y') {
			File_Manager::remove_contact(&contact);
			Calendar_Manager::remove_contact(contact);
			std::vector<Contact>::iterator it = contacts.begin() + index;
			contacts.erase(it);
		}
	}

	void modify_person(std::vector<Contact> & contacts, Contact & contact, int index) {
		char letter = '\0';
		while (letter != 'b') {
			cout << clear_screen(10) << contact.display() << "\n\n[E]dit     [R]emove     [S]cheduling     [B]ack\n" <<
					"Enter letter for selection: "; cin >> letter; flush_input();
			switch (letter) {
			case 'e' : case 'E' : edit_person(contact); break;
			case 'r' : case 'R' : remove_person(contacts, contact, index); return; break;
			case 's' : case 'S' : reschedule_person(contact); break;
			case 'b' : case 'B' : return; break;
			default : break;
			}
		}
	}

	bool contact_name_sort_options(std::array<string, 3> a, std::array<string, 3> b) {
		string str_a, str_b;
		if (a[0] != "") { str_a = a[0];
		} else if (a[1] != "") { str_a = a[1];
		} else { str_a = a[2];
		}
		if (b[0] != "") { str_b = b[0];
		} else if (b[1] != "") { str_b = b[1];
		} else { str_b = b[2];
		}
		if (str_a < str_b) { return true;
		} else if (str_a == str_b) {
			if (a[1] != "") { str_a = a[1];
			} else { str_a = a[2];
			}
			if (b[1] != "") { str_b = b[1];
			} else { str_b = b[2];
			}
			if (str_a < str_b) { return true;
			} else if (str_a == str_b) {
				return a[2] < b[2];
			} else { return false;
			}
		} else { return false;
		}
	}

	void sort_contacts(std::vector<Contact> & contacts) {
		int choice = 0;
		for (int aaa = 0; aaa < SORT_OPTIONS_NUM; aaa++) {
			cout << aaa+1 << ". " << SORT_STRINGS[aaa] << "\n";
		}
		cout << "How do you want to sort your network? ";
		cin >> choice; flush_input();
		switch (choice-1) {
		case FIRST_NAME : std::sort(contacts.begin(), contacts.end() ,
				[](Contact a, Contact b) {
					return contact_name_sort_options({a.first_name, a.last_name, a.username}, {b.first_name, b.last_name, b.username});
				});
			break;
		case LAST_NAME : std::sort(contacts.begin(), contacts.end() ,
				[](Contact a, Contact b) {
					return contact_name_sort_options({a.last_name, a.first_name, a.username}, {b.last_name, b.first_name, b.username});
				});
			break;
		case USERNAME : std::sort(contacts.begin(), contacts.end() ,
				[](Contact a, Contact b) {
					return contact_name_sort_options({a.username, a.first_name, a.last_name}, {b.username, b.first_name, b.last_name});
				});
			break;
		case LAST_CONTACT : std::sort(contacts.begin(), contacts.end() ,
				[](Contact a, Contact b) {
					return a.last_contact < b.last_contact;
				});
			break;
		case NEXT_CONTACT : std::sort(contacts.begin(), contacts.end() ,
				[](Contact a, Contact b) {
					return a.next_contact < b.next_contact;
				});
			break;
		default: break;
		}
	}

	void search_contacts(std::vector<Contact> contacts) {
		static unsigned count = 10;
		unsigned next_index = 0, choice = 0;
		int count_input = 0;
		char letter = '\0';
		while (letter != 'B') {
			cout << clear_screen(5);
			if (next_index + count >= contacts.size()) {
				next_index = contacts.size() - count;
			}
			if (next_index > contacts.size()) {
				next_index = 0;
			}
			for (unsigned aaa = 0; aaa < count; aaa++) {
				if ((next_index + aaa) >= contacts.size()) {
					break;
				}
				cout << endl << endl << (aaa+1) << ". " << contacts[next_index + aaa].display_short();
			}
			cout << endl << endl << "[P]revious     [C]ount: " << count << "     [S]ort     [B]ack     [N]ext     " <<
					"Contacts " << next_index + 1 << "-" << (count < contacts.size() ? (next_index + count) : contacts.size()) << " of " << contacts.size() <<
					endl << "Enter letter for selection or number to select a person: "; cin >> letter;
			if (std::isdigit(letter)) {
				cin.putback(letter); cin >> choice; flush_input();
				if (choice > 0 && choice <= count && choice <= contacts.size()) {
					modify_person(contacts, contacts[next_index + choice - 1], next_index + choice - 1);
					return;
				}
			} else {
				flush_input();
				switch (letter) {
				case 'p' : case 'P' : next_index -= count; break;
				case 'c' : case 'C' :
					for (int aaa = 0; aaa < 10; aaa++) {
						cout << "Enter new number for the count per page: ";
						cin >> count_input; flush_input();
						if (count_input <= 0) {
							cout << "Bad count value, must enter a positive integer" << endl;
						} else {
							count = count_input;
							break;
						}
					}
					break;
				case 's' : case 'S' : sort_contacts(contacts); break;
				case 'b' : letter = 'B'; case 'B' : break;
				case 'n' : case 'N' : next_index += count; break;
				default : break;
				}
			}
		}
	}

	void find_person() {
		std::vector<Contact> contacts; string line;
		cout << clear_screen();
		while (contacts.empty()) {
			cout << "Search Contacts: "; getline(cin, line);
			stringstream re_s; re_s << ".*";
			for (string::iterator it = line.begin(); it != line.end(); it++) {
				re_s << *it << ".*";
			}
			std::regex re(re_s.str(), std::regex::icase);
			contacts = File_Manager::get_all_contacts(&re);
			if (contacts.empty()) {
				if (line == "") {
					return;
				}
				cout << "Found no one. Try again." << endl;
			}
		}
		if (contacts.size() == 1) {
			modify_person(contacts, contacts[0], 0);
		} else {
			search_contacts(contacts);
		}
	}

	void display_network() {
		cout << clear_screen();
		std::vector<Contact> all_contacts = File_Manager::get_all_contacts();
		if (all_contacts.empty()) {
			cout << "Network List is Empty" << endl;
		} else {
			search_contacts(all_contacts);
		}
	}
}
