/*
 * wxPanel_Social_Networks.h
 *
 *  Created on: Dec 21, 2023
 *      Author: oldma
 */

#ifndef WXPANEL_SOCIAL_NETWORKS_H_
#define WXPANEL_SOCIAL_NETWORKS_H_

#include <string>
#include <vector>

#include "wxObjects_includes.h"

class wxPanel_Social_Networks : public wxPanel
{
	private:
		std::vector<std::string> social_network_nuids;

		void Move_Social_Network(int dir);
		void build();

	protected:
		wxButton* m_button_Edit_Social_Network;
		wxButton* m_button_Copy_Social_Network;
		wxButton* m_button_Add_Social_Network;
		wxButton* m_button_Delete_Social_Network;
		wxDataViewColumn* m_dataViewListColumn5;
		wxDataViewColumn* m_dataViewListColumn6;
		wxButton* m_button_Move_Down;
		wxButton* m_button_Move_Up;
		wxButton* m_button_Back;
		wxDataViewListCtrl* m_dataViewListCtrl_Social_Networks;

		void Edit( wxCommandEvent& event );
		void Copy_Social_Network( wxCommandEvent& event );
		void Add_Social_Network( wxCommandEvent& event );
		void Delete_Social_Network( wxCommandEvent& event );
		void Move_Down( wxCommandEvent& event );
		void Move_Up( wxCommandEvent& event );
		void Back( wxCommandEvent& event );
		void Selection_Changes( wxCommandEvent& event );

		void enable_buttons_check();

	public:

		wxPanel_Social_Networks(wxWindow * parent);

		~wxPanel_Social_Networks();
};

#endif /* WXPANEL_SOCIAL_NETWORKS_H_ */
