/*
 * Network_Maintainer_Contacts.h
 *
 *  Created on: Aug 5, 2023
 *      Author: oldma
 */

#ifndef SRC_NETWORK_MAINTAINER_CONTACTS_H_
#define SRC_NETWORK_MAINTAINER_CONTACTS_H_

namespace Network_Maintainer {
	void force_reschedule_all_contacts();
	void add_person();
	void add_persons_from_list();
	void find_person();
	void display_network();
}

#endif /* SRC_NETWORK_MAINTAINER_CONTACTS_H_ */
