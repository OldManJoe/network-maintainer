/*
 * wxFrame_Social_Network_Edit.cpp
 *
 *  Created on: Jan 25, 2024
 *      Author: oldma
 */


#include "wxDialog_Social_Network_Edit.h"

#include <string>

#include "File_Manager.h"
#include "Window_Manager.h"

wxDialog_Social_Network_Edit::wxDialog_Social_Network_Edit( wxWindow* parent) : wxDialog( parent, wxID_ANY, "Social Network", wxDefaultPosition, wxSize(280, 130))
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxGridBagSizer* gbSizer5;
	gbSizer5 = new wxGridBagSizer( 0, 0 );
	gbSizer5->SetFlexibleDirection( wxBOTH );
	gbSizer5->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	m_staticText15 = new wxStaticText( this, wxID_ANY, wxT("Name:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText15->Wrap( -1 );
	gbSizer5->Add( m_staticText15, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT|wxALL, 5 );

	m_textCtrl_Name = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	gbSizer5->Add( m_textCtrl_Name, wxGBPosition( 0, 1 ), wxGBSpan( 1, 3 ), wxALL|wxEXPAND, 5 );

	m_staticText16 = new wxStaticText( this, wxID_ANY, wxT("URL: "), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText16->Wrap( -1 );
	gbSizer5->Add( m_staticText16, wxGBPosition( 1, 0 ), wxGBSpan( 1, 1 ), wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT|wxALL, 5 );

	m_textCtrl_URL = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	gbSizer5->Add( m_textCtrl_URL, wxGBPosition( 1, 1 ), wxGBSpan( 1, 3 ), wxALL|wxEXPAND, 5 );

	gbSizer5->Add( 100, 0, wxGBPosition( 2, 2 ), wxGBSpan( 1, 1 ), wxEXPAND, 5 );

	m_button_Okay = new wxButton( this, wxID_OK, wxT("Okay"), wxDefaultPosition, wxDefaultSize, 0 );
	gbSizer5->Add( m_button_Okay, wxGBPosition( 2, 3 ), wxGBSpan( 1, 1 ), wxALIGN_RIGHT|wxALL, 5 );

	m_button_Cancel = new wxButton( this, wxID_CANCEL, wxT("Cancel"), wxDefaultPosition, wxDefaultSize, 0 );
	gbSizer5->Add( m_button_Cancel, wxGBPosition( 2, 0 ), wxGBSpan( 1, 2 ), wxALL, 5 );

	this->SetSizer( gbSizer5 );
	this->Layout();

	this->Centre( wxBOTH );
}

wxDialog_Social_Network_Edit::~wxDialog_Social_Network_Edit()
{

}

