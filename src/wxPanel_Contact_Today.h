/*
 * wxPanel_Contact_Today.h
 *
 *  Created on: Dec 21, 2023
 *      Author: oldma
 */

#ifndef WXPANEL_CONTACT_TODAY_H_
#define WXPANEL_CONTACT_TODAY_H_

#include "wxObjects_includes.h"

class wxPanel_Contact_Today : public wxPanel
{
	private:
		std::vector<std::string> contact_uuids;
		std::vector<std::string> greetings;
		std::vector<std::string> questions;

		void build();

	protected:
		wxStaticText* m_staticText1;
		wxButton* m_button_Prompt;
		wxButton* m_button_All;
		wxButton* m_button_Contacted;
		wxButton* m_button_Reschedule;
		wxButton* m_button_Details;
		wxDataViewColumn* m_dataViewListColumn1;
		wxDataViewColumn* m_dataViewListColumn2;
		wxDataViewColumn* m_dataViewListColumn3;
		wxButton* m_button_New_Contact;
		wxButton* m_button_All_Contacts;
		wxButton* m_button_Social_Networks;
		wxTextCtrl* m_textCtrl_Prompt;
		wxDataViewListCtrl* m_dataViewListCtrl_Daily_Contacts;

		void Refresh_Prompt( wxCommandEvent& event );
		void Select_All( wxCommandEvent& event );
		void Contacted( wxCommandEvent& event );
		void Reschedule( wxCommandEvent& event );
		void Details( wxCommandEvent& event );
		void New_Contact( wxCommandEvent& event );
		void All_Contacts( wxCommandEvent& event );
		void Social_Networks( wxCommandEvent& event );
		void Selection_Changes( wxCommandEvent& event );

		void enable_buttons_check();


	public:
		wxPanel_Contact_Today(wxWindow * parent);

		~wxPanel_Contact_Today();
};

#endif /* WXPANEL_CONTACT_TODAY_H_ */
