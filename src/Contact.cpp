/*
 * Contact.cpp
 *
 *  Created on: Jan 9, 2023
 *      Author: Robert Higgins
 */

#include "Contact.h"

#include <chrono>
#include <iomanip>
#include <sstream>

#include "Calendar_Manager.h"
#include "File_Manager.h"

using std::endl;
using std::string;
using std::stringstream;
using std::to_string;

Contact::Contact() {
	uuid = "";
	first_name = "";
	middle_name = "";
	last_name = "";
	username = "";
	frequency = 0;
	last_contact = std::chrono::year_month_day();
	next_contact = std::chrono::year_month_day();
	snid = "";
	enabled = false;
	missed_contact = false;
	notes = "";
}

Contact * Contact::open(string uuid) {
	if (this->uuid == uuid) return this;
	else return NULL;
}

Contact Contact::copy() {
	Contact new_contact;
	new_contact.first_name = this->first_name;
	new_contact.middle_name = this->middle_name;
	new_contact.last_name = this->last_name;
	new_contact.username = this->username;
	new_contact.frequency = this->frequency;
	new_contact.snid = this->snid;
	new_contact.enabled = this->enabled;
	new_contact.notes = this->notes;
	return new_contact;
}

void Contact::set_uuid() {
	if (this->uuid != "") return; // do nothing if the UUID has already been generated
	std::chrono::system_clock::time_point now;
	stringstream hash_stream; string string_to_hash;
	string::size_type hash_length = to_string(SIZE_MAX).length();
	do {
		now = std::chrono::system_clock::now();
		string_to_hash = this->first_name + this->middle_name + this->last_name + this->username + to_string(now.time_since_epoch().count());
		hash_stream.str(""); hash_stream << std::setw(hash_length) << std::setfill('0') << std::hash<string>()(string_to_hash);
	} while (File_Manager::contact_exists(hash_stream.str()));
	this->uuid = hash_stream.str();
}

void Contact::set_uuid(string uuid) {
	if (this->uuid != "") {
		return; // do nothing if the UUID has already been generated
	}
	this->uuid = uuid;
}

string Contact::get_uuid() const {
	return this->uuid;
}

string Contact::get_name() const {
	stringstream name;
	if (this->first_name != "") {
		name << this->first_name << " ";
	}
	if (this->middle_name != "") {
		name << this->middle_name << " ";
	}
	if (this->last_name != "") {
		name << this->last_name;
	}
	return name.str();
}

string Contact::get_social_network_name() const {
	return File_Manager::get_social_network_name(snid);
}

// displays all information
string Contact::display() const {
	stringstream display; display << "Name: " << get_name();
	display << endl <<
			"Username: " << this->username << endl <<
			"Network: " << get_social_network_name() << endl <<
			"Enabled: " << (this->enabled ? "True " : "False");
	if (this->enabled) {
		display << "\t\t\tFrequency: ";
		if (this->frequency > 0) {
			display << Calendar_Manager::FREQUENCY_STRINGS[this->frequency];
		} else {
			display << Calendar_Manager::FREQUENCY_STRINGS[0] << ": " << to_string(-(this->frequency)) << " days";
		}
		display << endl;
	}
	if (this->enabled) {
		if (last_contact != std::chrono::year_month_day()) {
			display << "Last Contact: " << std::setw(4) << std::setfill('0') << static_cast<int>(this->last_contact.year()) << "-" <<
					std::setw(2) << std::setfill('0') << static_cast<unsigned>(this->last_contact.month()) << "-" <<
					std::setw(2) << std::setfill('0') << static_cast<unsigned>(this->last_contact.day());
			display << "\t";
		}
		if (next_contact != std::chrono::year_month_day()) {
			display << "Next Contact: " << std::setw(4) << std::setfill('0') << static_cast<int>(this->next_contact.year()) << "-" <<
					std::setw(2) << std::setfill('0') << static_cast<unsigned>(this->next_contact.month()) << "-" <<
					std::setw(2) << std::setfill('0') << static_cast<unsigned>(this->next_contact.day());
		} else { display << "Next Contact: not set";
		}
	}
	display << endl << "Notes: " << this->notes;
	return display.str();
}

// displays name, username, network, and next contact
string Contact::display_short() const {
	stringstream display, name;
	if (this->first_name != "") {
		name << this->first_name << " ";
	}
	if (this->middle_name != "") {
		name << this->middle_name << " ";
	}
	if (this->last_name != "") {
		name << this->last_name;
	}
	if (name.str() != "") {
		display << "Name: " << name.str() << endl;
	}
	if (this->username != "") {
		display << "Username: " << this->username << endl;
	}
	display << "Network: " << File_Manager::get_social_network_name(snid) << endl;
	if (this->enabled) {
		if (next_contact != std::chrono::year_month_day()) {
		display << "Next Contact: " << std::setw(4) << std::setfill('0') << to_string(static_cast<int>(this->next_contact.year())) << "-" <<
				std::setw(2) << std::setfill('0') << to_string(static_cast<unsigned>(this->next_contact.month())) << "-" <<
				std::setw(2) << std::setfill('0') << to_string(static_cast<unsigned>(this->next_contact.day()));
		} else {
			display << "Next Contact: not set";
		}
	} else {
		display << "Disabled";
	}
	return display.str();
}
