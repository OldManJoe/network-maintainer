/*
 * Network_Maintainer.h
 *
 *  Created on: Aug 26, 2022
 *      Author: Robert Higgins
 */

#ifndef NETWORK_MAINTAINER_H_
#define NETWORK_MAINTAINER_H_

#include <string>

#include "Global_Variables.h"

namespace Network_Maintainer {
	void print(std::string print);
	void throw_flag(exception_flag_enum flag);
	void throw_exception(const std::exception & e);

	void flush_input();
	std::string clear_screen(int num_lines = 30);
}
#endif /* NETWORK_MAINTAINER_H_ */
