/*
 * wxPanel_All_Contacts.cpp
 *
 *  Created on: Dec 21, 2023
 *      Author: oldma
 */

#include "wxPanel_All_Contacts.h"

#include <array>
#include <sstream>

#include "Calendar_Manager.h"
#include "File_Manager.h"
#include "Window_Manager.h"

using std::string;

wxArrayString m_choice_Sort_OptionChoices = {"First Name", "Last Name", "Username", "Last Contact", "Next Contact"};

bool contact_name_sort_options(std::array<string, 3> a, std::array<string, 3> b) {
	string str_a, str_b;

	if((a[0] == "") != (b[0] == "")) {
		return b[0] == "";
	}

	if (a[0] != "") { // set string a to the first non-empty item
		str_a = a[0];
	} else if (a[1] != "") {
		str_a = a[1];
	} else {
		str_a = a[2];
	}

	if (b[0] != "") { // set string b to the first non-empty item
		str_b = b[0];
	} else if (b[1] != "") {
		str_b = b[1];
	} else {
		str_b = b[2];
	}

	if (str_a < str_b) { // compare the strings
		return true;
	} else if (str_a == str_b) { // if they are equal, compare following items from the set
		if (a[1] != "") {
			str_a = a[1];
		} else {
			str_a = a[2];
		}

		if (b[1] != "") {
			str_b = b[1];
		} else {
			str_b = b[2];
		}

		if (str_a < str_b) { // compare the strings
			return true;
		} else if (str_a == str_b) { // if they are equal, compare following items from the set
			return a[2] < b[2];
		} else {
			return false;
		}
	} else {
		return false;
	}
}

wxPanel_All_Contacts::wxPanel_All_Contacts(wxWindow * parent, sort_enum sort_option) :  wxPanel(parent, wxID_ANY)
{
	int spacer_width = 70;

	this->SetSizeHints( wxSize( 800,600 ), wxDefaultSize );

	wxGridBagSizer* gbSizer3;
	gbSizer3 = new wxGridBagSizer( 0, 0 );
	gbSizer3->SetFlexibleDirection( wxBOTH );
	gbSizer3->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	m_staticText2 = new wxStaticText( this, wxID_ANY, wxT("Find Contact:"), wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT );
	m_staticText2->Wrap( -1 );
	gbSizer3->Add( m_staticText2, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT|wxALL, 5 );

	m_textCtrl_Find_Contact = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	gbSizer3->Add( m_textCtrl_Find_Contact, wxGBPosition( 0, 1 ), wxGBSpan( 1, 7 ), wxALL|wxEXPAND, 5 );

	m_button_Find_Contact = new wxButton( this, wxID_ANY, wxT("Find"), wxDefaultPosition, wxDefaultSize, 0 );
	gbSizer3->Add( m_button_Find_Contact, wxGBPosition( 0, 8 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	m_button_Details = new wxButton( this, wxID_ANY, wxT("Details"), wxDefaultPosition, wxDefaultSize, 0 );
	gbSizer3->Add( m_button_Details, wxGBPosition( 1, 1 ), wxGBSpan( 1, 1 ), wxALIGN_CENTER|wxALIGN_CENTER_HORIZONTAL|wxALL, 5 );
	m_button_Details->Enable(false);

	gbSizer3->Add( spacer_width, 0, wxGBPosition( 1, 2 ), wxGBSpan( 1, 1 ), wxEXPAND, 5 );

	m_button_New_Contact = new wxButton( this, wxID_ANY, wxT("New Contact"), wxDefaultPosition, wxDefaultSize, 0 );
	gbSizer3->Add( m_button_New_Contact, wxGBPosition( 1, 3 ), wxGBSpan( 1, 1 ), wxALIGN_CENTER|wxALIGN_CENTER_HORIZONTAL|wxALL, 5 );

	gbSizer3->Add( spacer_width, 0, wxGBPosition( 1, 4 ), wxGBSpan( 1, 1 ), wxEXPAND, 5 );

	m_button_Copy_Contact = new wxButton( this, wxID_ANY, wxT("Copy Contact"), wxDefaultPosition, wxDefaultSize, 0 );
	gbSizer3->Add( m_button_Copy_Contact, wxGBPosition( 1, 5 ), wxGBSpan( 1, 1 ), wxALIGN_CENTER|wxALIGN_CENTER_HORIZONTAL|wxALL, 5 );
	m_button_Copy_Contact->Enable(false);

	gbSizer3->Add( spacer_width, 0, wxGBPosition( 1, 6 ), wxGBSpan( 1, 1 ), wxEXPAND, 5 );

	m_button_Delete_Contact = new wxButton( this, wxID_ANY, wxT("Delete Contact"), wxDefaultPosition, wxDefaultSize, 0 );
	gbSizer3->Add( m_button_Delete_Contact, wxGBPosition( 1, 7 ), wxGBSpan( 1, 1 ), wxALIGN_CENTER|wxALIGN_CENTER_HORIZONTAL|wxALL, 5 );
	m_button_Delete_Contact->Enable(false);

	m_dataViewListCtrl_Contacts = new wxDataViewListCtrl( this, wxID_ANY, wxDefaultPosition, wxSize( -1,-1 ), 0 );
	m_dataViewListCtrl_Contacts->SetMinSize( wxSize( 775,400 ) );

	m_dataViewListColumn7 = m_dataViewListCtrl_Contacts->AppendTextColumn( wxT("Name"), wxDATAVIEW_CELL_INERT, 300, static_cast<wxAlignment>(wxALIGN_LEFT), wxDATAVIEW_COL_RESIZABLE );
	m_dataViewListColumn8 = m_dataViewListCtrl_Contacts->AppendTextColumn( wxT("Username"), wxDATAVIEW_CELL_INERT, 300, static_cast<wxAlignment>(wxALIGN_LEFT), wxDATAVIEW_COL_RESIZABLE );
	m_dataViewListColumn9 = m_dataViewListCtrl_Contacts->AppendTextColumn( wxT("Next Contact"), wxDATAVIEW_CELL_INERT, 100, static_cast<wxAlignment>(wxALIGN_LEFT), wxDATAVIEW_COL_RESIZABLE );
	gbSizer3->Add( m_dataViewListCtrl_Contacts, wxGBPosition( 2, 0 ), wxGBSpan( 1, 9 ), wxALL, 5 );

	m_button_Reschedule_All = new wxButton( this, wxID_ANY, wxT("Reschedule All Contacts"), wxDefaultPosition, wxDefaultSize, 0 );
	gbSizer3->Add( m_button_Reschedule_All, wxGBPosition( 3, 0 ), wxGBSpan( 1, 2 ), wxALIGN_LEFT|wxALL, 5 );

	m_staticText3 = new wxStaticText( this, wxID_ANY, wxT("Sort:"), wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT );
	m_staticText3->Wrap( -1 );
	gbSizer3->Add( m_staticText3, wxGBPosition( 3, 3 ), wxGBSpan( 1, 1 ), wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT|wxALL, 5 );

	m_choice_Sort_Option = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_choice_Sort_OptionChoices, 0 );
	gbSizer3->Add( m_choice_Sort_Option, wxGBPosition( 3, 4 ), wxGBSpan( 1, 2 ), wxALL, 5 );

	m_button_Back = new wxButton( this, wxID_ANY, wxT("Back"), wxDefaultPosition, wxDefaultSize, 0 );
	gbSizer3->Add( m_button_Back, wxGBPosition( 3, 8 ), wxGBSpan( 1, 1 ), wxALIGN_RIGHT|wxALL, 5 );

	this->SetSizer( gbSizer3 );
	this->Layout();

	this->Centre( wxBOTH );

	// Connect Events
	m_button_Find_Contact->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_All_Contacts::Find_Contact ), NULL, this );
	m_button_Details->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_All_Contacts::Contact_Details ), NULL, this );
	m_button_New_Contact->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_All_Contacts::New_Contact ), NULL, this );
	m_button_Copy_Contact->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_All_Contacts::Copy_Contact ), NULL, this );
	m_button_Delete_Contact->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_All_Contacts::Delete_Contact ), NULL, this );
	m_button_Reschedule_All->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_All_Contacts::Reschedule_All_Contacts ), NULL, this );
	m_choice_Sort_Option->Connect( wxEVT_CHOICE, wxCommandEventHandler( wxPanel_All_Contacts::Sort ), NULL, this );
	m_button_Back->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_All_Contacts::Back ), NULL, this );
	m_dataViewListCtrl_Contacts->Connect( wxEVT_DATAVIEW_SELECTION_CHANGED, wxCommandEventHandler( wxPanel_All_Contacts::Selection_Changes ), NULL, this );

	build(sort_option);
}

wxPanel_All_Contacts::~wxPanel_All_Contacts()
{
	// Disconnect Events
	m_button_Find_Contact->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_All_Contacts::Find_Contact ), NULL, this );
	m_button_Details->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_All_Contacts::Contact_Details ), NULL, this );
	m_button_New_Contact->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_All_Contacts::New_Contact ), NULL, this );
	m_button_Copy_Contact->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_All_Contacts::Copy_Contact ), NULL, this );
	m_button_Delete_Contact->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_All_Contacts::Delete_Contact ), NULL, this );
	m_button_Reschedule_All->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_All_Contacts::Reschedule_All_Contacts ), NULL, this );
	m_choice_Sort_Option->Disconnect( wxEVT_CHOICE, wxCommandEventHandler( wxPanel_All_Contacts::Sort ), NULL, this );
	m_button_Back->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( wxPanel_All_Contacts::Back ), NULL, this );
	m_dataViewListCtrl_Contacts->Disconnect( wxEVT_DATAVIEW_SELECTION_CHANGED, wxCommandEventHandler( wxPanel_All_Contacts::Selection_Changes ), NULL, this );
}

void wxPanel_All_Contacts::load_contacts(std::vector<Contact> all_contacts) {
	m_dataViewListCtrl_Contacts->DeleteAllItems();
	contact_uuids.clear();
	wxVector<wxVariant> data;
	for (Contact contact : all_contacts) {
		data.clear();
		data.push_back(contact.get_name());
		data.push_back(contact.username);
		std::stringstream date;
		date << std::setw(4) << std::setfill('0') << static_cast<int>(contact.next_contact.year()) << "-" <<
				std::setw(2) << std::setfill('0') << static_cast<unsigned>(contact.next_contact.month()) << "-" <<
				std::setw(2) << std::setfill('0') << static_cast<unsigned>(contact.next_contact.day());
		if (date.str() == "0000-00-00") {
			data.push_back("");
		} else {
			data.push_back(date.str());
		}
		contact_uuids.push_back(contact.get_uuid());
		m_dataViewListCtrl_Contacts->AppendItem(data);
	}
}

void wxPanel_All_Contacts::build(sort_enum sort_option) {
	std::vector<Contact> all_contacts = File_Manager::get_all_contacts();
	m_choice_Sort_Option->SetSelection(sort_option);
	load_contacts(all_contacts);
	wxCommandEvent null_event = wxCommandEvent();
	Sort(null_event);
}

void wxPanel_All_Contacts::Find_Contact( wxCommandEvent& event ) {
	std::stringstream re_s; re_s << ".*";
	string line = m_textCtrl_Find_Contact->GetValue().ToStdString();
	for (string::iterator it = line.begin(); it != line.end(); it++) {
		re_s << *it << ".*";
	}
	std::regex re(re_s.str(), std::regex::icase);
	std::vector<Contact> all_contacts = File_Manager::get_all_contacts(&re);
	load_contacts(all_contacts);
}

void wxPanel_All_Contacts::Contact_Details( wxCommandEvent& event ) {
	int row_id = m_dataViewListCtrl_Contacts->GetSelectedRow();
	if (row_id == wxNOT_FOUND) {
		return;
	}
	string contact_uuid = contact_uuids[row_id];
	Contact contact = File_Manager::get_contact(contact_uuid);
	Window_Manager::Contact_Details(&contact, true);
}

void wxPanel_All_Contacts::New_Contact( wxCommandEvent& event ) {
	Contact contact;
	Window_Manager::Contact_Details(&contact, false);
}

void wxPanel_All_Contacts::Copy_Contact( wxCommandEvent& event ) {
	int row_id = m_dataViewListCtrl_Contacts->GetSelectedRow();
	if (row_id == wxNOT_FOUND) {
		return;
	}
	string contact_uuid = contact_uuids[row_id];
	Contact contact = File_Manager::get_contact(contact_uuid).copy();
	Window_Manager::Contact_Details(&contact, false);
}

void wxPanel_All_Contacts::Delete_Contact( wxCommandEvent& event ) {
	int row_id = m_dataViewListCtrl_Contacts->GetSelectedRow();
	if (row_id == wxNOT_FOUND) {
		return;
	}
	wxMessageDialog yes_no_dialog(this,
			"This will permanently delete this Contact. Are you sure you want to delete this contact?",
			"Delete Contact?",
			wxYES_NO);
	if (yes_no_dialog.ShowModal() != wxID_YES) {
		return;
	}
	string contact_uuid = contact_uuids[row_id];
	Contact contact = File_Manager::get_contact(contact_uuid);
	File_Manager::remove_contact(&contact);
	Calendar_Manager::remove_contact(contact);
	m_dataViewListCtrl_Contacts->DeleteItem(row_id);
	std::vector<string>::iterator it = contact_uuids.begin() + row_id;
	contact_uuids.erase(it);

	enable_buttons_check();
}

void wxPanel_All_Contacts::Reschedule_All_Contacts( wxCommandEvent& event ) {
	wxTextEntryDialog reschedule_all_text_dialog(this,
			"Are you sure you want to reschedule all contacts for their next contact period?\n"
			"This cannot be undone. Please type CONFIRM: ");
	int response = reschedule_all_text_dialog.ShowModal();
	if (response != wxID_OK) {
		return;
	} else if (reschedule_all_text_dialog.GetValue() != "CONFIRM") {
		wxMessageDialog ok_dialog(this, "Incorrect confirmation, contacts not rescheduled");
		ok_dialog.ShowModal();
		return;
	}
	std::vector<Contact> all_contacts = File_Manager::get_all_contacts();
	for (Contact contact : all_contacts) {
		if(contact.enabled) {
			Calendar_Manager::set_next_contact(contact, false, true, false);
		}
	}
	wxMessageDialog ok_dialog(this, "Rescheduling all contacts");
	ok_dialog.ShowModal();
	load_contacts(File_Manager::get_all_contacts());
	wxCommandEvent null_event = wxCommandEvent();
	Sort(null_event);
}

void wxPanel_All_Contacts::Sort( wxCommandEvent& event ) {
	std::vector<Contact> contacts;
	for (string uuid : contact_uuids) {
		 contacts.push_back(File_Manager::get_contact(uuid));
	}
	sort_enum sort_option = (sort_enum) m_choice_Sort_Option->GetSelection();
	Window_Manager::set_sort_option(sort_option);
	switch (sort_option) {
	case FIRST_NAME : std::sort(contacts.begin(), contacts.end() ,
			[](Contact a, Contact b) {
				return contact_name_sort_options({a.first_name, a.last_name, a.username}, {b.first_name, b.last_name, b.username});
			});
		break;
	case LAST_NAME : std::sort(contacts.begin(), contacts.end() ,
			[](Contact a, Contact b) {
				return contact_name_sort_options({a.last_name, a.first_name, a.username}, {b.last_name, b.first_name, b.username});
			});
		break;
	case USERNAME : std::sort(contacts.begin(), contacts.end() ,
			[](Contact a, Contact b) {
				return contact_name_sort_options({a.username, a.first_name, a.last_name}, {b.username, b.first_name, b.last_name});
			});
		break;
	case LAST_CONTACT : std::sort(contacts.begin(), contacts.end() ,
			[](Contact a, Contact b) {
				return a.last_contact < b.last_contact;
			});
		break;
	case NEXT_CONTACT : std::sort(contacts.begin(), contacts.end() ,
			[](Contact a, Contact b) {
				return a.next_contact < b.next_contact;
			});
		break;
	default: break;
	}
	load_contacts(contacts);
}

void wxPanel_All_Contacts::Back( wxCommandEvent& event ) {
	Window_Manager::panel_back();
}

void wxPanel_All_Contacts::Selection_Changes( wxCommandEvent& event ) {
	enable_buttons_check();
}

void wxPanel_All_Contacts::enable_buttons_check() {
	bool enable_buttons = m_dataViewListCtrl_Contacts->GetSelectedItemsCount() == 1;
	m_button_Details->Enable(enable_buttons);
	m_button_Copy_Contact->Enable(enable_buttons);
	m_button_Delete_Contact->Enable(enable_buttons);
}
