/*
 * Social_Network.cpp
 *
 *  Created on: Jan 30, 2023
 *      Author: Robert Higgins
 */

#include "Social_Network.h"

#include <chrono>
#include <iomanip>
#include <sstream>

#include "File_Manager.h"

using std::string;
using std::to_string;

Social_Network::Social_Network() {
	nuid = "";
	order = 0;
	network_name = "";
	url = "";
}

void Social_Network::set_nuid() {
	if (this->nuid != "") return; // do nothing if the NUID has already been generated
	std::chrono::system_clock::time_point now;
	std::stringstream hash_stream; string string_to_hash;
	string::size_type hash_length = to_string(SIZE_MAX).length();
	do {
		now = std::chrono::system_clock::now();
		string_to_hash = this->network_name + this->url + to_string(now.time_since_epoch().count());
		hash_stream.str(""); hash_stream << std::setw(hash_length) << std::setfill('0') << std::hash<string>()(string_to_hash);
	} while (File_Manager::social_network_exists(hash_stream.str()));
	this->nuid = hash_stream.str();
}

void Social_Network::set_nuid(string nuid) {
	if (this->nuid != "") return; // do nothing if the UUID has already been generated
	this->nuid = nuid;
}

string Social_Network::get_nuid() const {
	return this->nuid;
}

string Social_Network::display() const {
	std::stringstream display;
	display << std::setw(30) << std::left << this->network_name << (this->url == "" ? "" : "URL: ") << this->url;
	return display.str();
}

