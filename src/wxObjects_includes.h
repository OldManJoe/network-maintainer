/*
 * wxObjects_includes.h
 *
 *  Created on: Dec 21, 2023
 *      Author: oldma
 */

#ifndef WXOBJECTS_INCLUDES_H_
#define WXOBJECTS_INCLUDES_H_

#include <wx/artprov.h>
#include <wx/bitmap.h>
#include <wx/button.h>
#include <wx/checkbox.h>
#include <wx/choice.h>
#include <wx/colour.h>
#include <wx/dataview.h>
#include <wx/dialog.h>
#include <wx/font.h>
#include <wx/frame.h>
#include <wx/gbsizer.h>
#include <wx/gdicmn.h>
#include <wx/icon.h>
#include <wx/image.h>
#include <wx/msgdlg.h>
#include <wx/settings.h>
#include <wx/stattext.h>
#include <wx/string.h>
#include <wx/textctrl.h>
#include <wx/textdlg.h>

#include <wx/xrc/xmlres.h>

#endif /* WXOBJECTS_INCLUDES_H_ */
