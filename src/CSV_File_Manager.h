/*
 * CSV_File_Manager.h
 *
 *  Created on: Mar 2, 2023
 *      Author: Robert Higgins
 */

#ifndef CSV_FILE_MANAGER_H_
#define CSV_FILE_MANAGER_H_

#include <regex>
#include <string>
#include <vector>

#include "Contact.h"
#include "File_Manager.h"
#include "Social_Network.h"

namespace CSV_File_Manager {
	extern char csv_separator;

	void check_files();

	// Contact specific commands
	std::vector<Contact> get_all_contacts(std::regex * re = NULL);
	Contact get_contact(std::string uuid);

	// Social_Network specific commands
	std::vector<Social_Network> get_all_social_networks(std::regex * re = NULL);
	Social_Network get_social_network(std::string nuid);
	std::string get_social_network_name(std::string nuid);

	// Generic commands
	bool exists(File_Manager::command_argument_enum object_type, std::string id);
	bool add_update_remove(File_Manager::command_enum command, File_Manager::command_argument_enum object_type, void * object);
}

#endif /* CSV_FILE_MANAGER_H_ */
