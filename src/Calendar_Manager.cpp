/*
 * Calendar_Manager.cpp
 *
 *  Created on: Oct 12, 2022
 *      Author: Robert Higgins
 */

#include "Calendar_Manager.h"

#include <chrono>
#include <condition_variable>
#include <fstream>
#include <filesystem>
#include <iomanip>
#include <map>
#include <mutex>
#include <queue>
#include <random>
#include <sstream>
#include <string>
#include <thread>
#include <vector>

#include "Contact.h"
#include "File_Manager.h"
#include "Global_Variables.h"
#include "Logging.h"
#include "Network_Maintainer.h"

using std::endl;
using std::string;
using std::stringstream;
using std::to_string;
using std::chrono::local_days;
using std::chrono::year_month_day;

namespace Calendar_Manager {
	// public variables
	std::filesystem::path calendar_filename = std::filesystem::path("../data/calendar.cal");

	// private constants, enums, structs, and classes
	enum command_enum {QUIT, WRITE, SET_NEXT_CONTACT, RESET_NEXT_CONTACT, REMOVE_CONTACT, CONTACT_TODAY};

	struct Command {
		int command;
		string str;
		Contact contact;
		bool set_last_contact;
		bool force;
		bool automatic;
		void * reply;
		std::mutex * reply_mutex;
	};

	// private variables
	bool write_command_exists = false;
	// picking January 1st, 2001 because it starts on a Monday, weekEND not weekends
	local_days todays_date, base_date = local_days(std::chrono::year(2001)/1/1);
	const std::chrono::tzdb & tzdb = std::chrono::get_tzdb();
	const std::chrono::time_zone * time_zone = std::chrono::current_zone();
	std::map<local_days, std::vector<string>> assignments;

	// thread control
	std::thread calendar_manager_thread;
	std::queue<Command> command_queue;
	std::mutex queue_mutex;
	std::condition_variable not_empty;

	// helper methods
	int local_days_to_date_int(local_days date) {
		year_month_day _date(date);
		return static_cast<int>(_date.year()) * 10000 +
				static_cast<unsigned>(_date.month()) * 100 +
				static_cast<unsigned>(_date.day());
	}

	local_days date_int_to_local_days(int date_int) {
		return local_days(std::chrono::year(date_int / 10000)/((date_int % 10000) / 100)/(date_int % 100));
	}

	void read_assignments() {
		Logging::log("Calendar Manager Reading: " + calendar_filename.string());
		std::ifstream calendar_file(calendar_filename);
		std::vector<string> uuids; string uuid;
		int date = 0, count = 0;
		calendar_file.peek(); // if file empty prevent the while loop
		while (calendar_file) {
			calendar_file >> date >> count;
			for (int aaa = 0; aaa < count; aaa++) { calendar_file >> uuid; uuids.push_back(uuid);
			}
			assignments[date_int_to_local_days(date)] = uuids;
			uuids.clear();
		}
	}

	void missed_contacts() {
		Logging::log("Calendar Manager rescheduling missed contacts");
		std::map<local_days, std::vector<string>>::iterator it = assignments.begin();
		while (it->first < todays_date && it != assignments.end()) {
			for (string uuid : it->second) {
				Contact contact = File_Manager::get_contact(uuid);
				reset_next_contact(contact, false, false, true);
			}
			it++;
		}
	}

	void check_file(std::filesystem::path file, string default_lines) {
		std::ifstream file_open; file_open.open(file);
		if (!file_open) {
			std::filesystem::path directory = file;
			directory.remove_filename();
			if (!std::filesystem::is_directory(directory)) { std::filesystem::create_directory(directory);
			}
			std::ofstream file_write(file);
			if (file_write) { file_write << default_lines;
			} else { Logging::log("FILE_EXCEPTION: could not create " + file.string()); throw FILE_EXCEPTION;
			}
		}
	}

	void check_files() {
		check_file(calendar_filename, "");
	}

	int month_frequency_modifier(std::chrono::month month, int num_months, bool next_period) {
		unsigned m = static_cast<unsigned>(month);
		return ((m + (num_months * next_period) - 1) / num_months) % (12 / num_months);
	}

	std::vector<unsigned> get_assignment_lengths(local_days start_date, int length) {
		using std::chrono::days;
		std::vector<unsigned> assignment_lengths;
		local_days index_date = start_date;
		std::map<local_days, std::vector<string>>::iterator it = assignments.lower_bound(index_date);
		if (it == assignments.end()) {
			for (int aaa = 0; aaa < length; aaa++) {
				assignment_lengths.push_back(0);
			}
			return assignment_lengths;
		}
		local_days end_date = start_date + days(length);
		days distance = it->first - index_date;
		if (distance > days(0)) {
			assignment_lengths.push_back(0);
		}
		for ( ; (it->first < end_date && it != assignments.end()); it++) {
			distance = it->first - index_date;
			for (int aaa = 1; days(aaa) < distance; aaa++) {
				assignment_lengths.push_back(0);
			}
			assignment_lengths.push_back(it->second.size());
			index_date = it->first;
		}
		distance = end_date - index_date;
		for (int aaa = 1; days(aaa) < distance; aaa++) {
			assignment_lengths.push_back(0);
		}
		return assignment_lengths;
	}

	std::vector<unsigned> get_assignment_possibilities(std::vector<unsigned> & assignment_lengths) {
		std::vector<unsigned> assignment_possibilities;
		unsigned min_assigned = assignment_lengths[0];
		assignment_possibilities.push_back(0);
		for (unsigned aaa = 1; aaa < assignment_lengths.size(); aaa++) {
			if (assignment_lengths[aaa] < min_assigned) {
				assignment_possibilities = {aaa};
				min_assigned = assignment_lengths[aaa];
			} else if (assignment_lengths[aaa] == min_assigned) {
				assignment_possibilities.push_back(aaa);
			}
		}
		return assignment_possibilities;
	}

	// public methods
	string write_settings() {
		stringstream settings;
		if (base_date != local_days(std::chrono::year(2001)/1/1)) {
			settings << "base_date " << local_days_to_date_int(base_date) << endl;
		}
		settings << "time_zone " << time_zone->name() << endl;
		return settings.str();
	}

	bool set_time_zone(std::string tz) {
		try {
			time_zone = tzdb.locate_zone(tz);
			return true;
		} catch (std::runtime_error &e) {
			return false;
		}
	}

	bool set_time_zone(const std::chrono::time_zone * tz) {
		time_zone = tz;
		todays_date = std::chrono::floor<std::chrono::days>(time_zone->to_local(std::chrono::system_clock::now()));
		return true;
	}

	const std::chrono::time_zone * get_time_zone() {
		return time_zone;
	}

	const std::chrono::tzdb & get_tzdb() {
		return tzdb;
	}

	bool set_base_date(std::string date) {
		using std::chrono::day;
		using std::chrono::month;
		using std::chrono::year;
		if (date.size() < 8) {
			return false;
		}
		year_month_day date_ymd = year_month_day(
				year(stoi(date.substr(0,4))),
				month(stoi(date.substr(4,2))),
				day(stoi(date.substr(6,2))));
		if (date_ymd.ok()) {
			base_date = local_days(date_ymd); return true;
		} else {
			return false;
		}
	}

	year_month_day get_base_date_ymd() {
		return year_month_day(base_date);
	}

	year_month_day get_todays_date() {
		return year_month_day(todays_date);
	}

	// Commands
	Command get_command() {
		if (DEBUG) { Logging::log("Calendar_Manager::get_command()");
		}
		std::unique_lock<std::mutex> lock(queue_mutex);
		not_empty.wait(lock, [](){return !command_queue.empty();});
		Command current_command;
		do {
			current_command = command_queue.front(); command_queue.pop();
			if (current_command.command == WRITE) {
				if (!command_queue.empty()) { command_queue.push(current_command);
				} else { break;
				}
			}
		} while (current_command.command == WRITE);
		return current_command;
	}

	void send_command(Command command) {
		std::unique_lock<std::mutex> lock(queue_mutex);
		command_queue.push(command);
		not_empty.notify_one();
	}

	void _write() {
		Logging::log("Calendar Manager Writing to: " + calendar_filename.string());

		std::filesystem::path calendar_temp_filename = calendar_filename;
		calendar_temp_filename += ".tmp";
		std::ofstream calendar_temp_file(calendar_temp_filename);

		std::map<local_days, std::vector<string>>::iterator it = assignments.lower_bound(todays_date);
		while (it != assignments.end()) {
			if (it->second.size() > 0) {
				calendar_temp_file << local_days_to_date_int(it->first) << " " << it->second.size();
				for (string uuid : it->second) {
					calendar_temp_file << " " << uuid;
				}
				calendar_temp_file << endl;
			}
			it++;
		}
		calendar_temp_file.close();
		std::filesystem::rename(calendar_temp_filename, calendar_filename);
		write_command_exists = false;
	}

	void _set_next_contact(Command command) {
		if (DEBUG) {
			Logging::log("Calendar_Manager::_set_next_contact(Command command)");
		}
		if (!command.contact.enabled || command.contact.frequency == CUSTOM ||
				(local_days(command.contact.next_contact) > todays_date && !command.force)) {
			if (command.reply_mutex != NULL) { command.reply_mutex->unlock();
			}
			return;
		}
		using std::chrono::days;
		using std::chrono::months;
		using std::chrono::years;
		year_month_day today_ymd(todays_date);
		if (command.set_last_contact) {
			command.contact.last_contact = today_ymd;
		}
		int length, mod = 0;
		local_days start_date, end_date;
		switch (command.contact.frequency) {
		case WEEKLY :
			length = 7;
			start_date = todays_date + days(7 - ((todays_date - base_date).count() % 7));
			break;
		case BIWEEKLY :
			length = 14;
			start_date = todays_date + days(14 - ((todays_date - base_date).count() % 14));
			break;
		case MONTHLY :
			start_date = local_days(today_ymd.year()/(today_ymd.month() + months(1))/1);
			end_date = local_days(year_month_day(start_date) + months(1));
			length = (end_date - start_date).count();
			break;
		case BIMONTHLY : mod = 2; break;
		case QUARTERLY : mod = 3; break;
		case TRIANNUALLY : mod = 4; break;
		case SEMIANNUALLY : mod = 6; break;
		case ANNUALLY :
			start_date = local_days((today_ymd.year() + years(1))/1/1);
			end_date = local_days(year_month_day(start_date) + years(1));
			length = (end_date - start_date).count();
			break;
		default :
			length = -command.contact.frequency;
			start_date = todays_date + days(length - ((todays_date - base_date).count() % length));
			break;
		}
		if (mod > 0) {
			start_date = local_days(today_ymd.year()/(month_frequency_modifier(today_ymd.month(), mod, true) * mod + 1)/1);
			if (start_date < todays_date) {
				start_date = local_days(year_month_day(start_date) + years(1));
			}
			end_date = local_days(year_month_day(start_date) + months(mod));
			length = (end_date - start_date).count();
		}
		std::vector<unsigned> assignment_lengths = get_assignment_lengths(start_date, length);
		std::vector<unsigned> assignment_possibilities = get_assignment_possibilities(assignment_lengths);
		std::uniform_int_distribution uid(0, static_cast<int>(assignment_possibilities.size() - 1));
		unsigned index = assignment_possibilities[uid(mt)];
		local_days next_contact = start_date + days(index);
		assignments[next_contact].push_back(command.contact.get_uuid());
		command.contact.next_contact = year_month_day(next_contact);
		File_Manager::update_contact(&command.contact, false);
		if (!write_command_exists) {
			Command c; c.command = WRITE; Calendar_Manager::send_command(c);
			write_command_exists = true;
		}
		if (command.reply != NULL) {
			((Contact *)command.reply)->next_contact = year_month_day(next_contact);
		}
		if (command.reply_mutex != NULL) {
			command.reply_mutex->unlock();
		}
	}

	void set_next_contact(Contact & contact, bool set_last_contact, bool force, bool wait_for_return) {
		if (DEBUG) {
			Logging::log("Calendar_Manager::set_next_contact(Contact & contact, bool set_last_contact, bool force, bool wait_for_return)");
		}
		if (contact.next_contact != year_month_day()) {
			remove_contact(contact);
		}
		Command c;
		c.command = SET_NEXT_CONTACT;
		c.set_last_contact = set_last_contact;
		c.force = force;
		c.contact = Contact(contact); c.reply = (wait_for_return ? &contact : NULL);
		std::mutex reply_mutex; c.reply_mutex = (wait_for_return ? &reply_mutex : NULL);
		if (wait_for_return) {
			reply_mutex.lock();
		}
		send_command(c);
		if (wait_for_return) {
			reply_mutex.lock();
		}
	}

	void _reset_next_contact(Command command) {
		if (DEBUG) {
			Logging::log("Calendar_Manager::_reset_next_contact(Command command)");
		}
		if (!command.contact.enabled || command.contact.frequency == CUSTOM ||
				(local_days(command.contact.next_contact) > todays_date && !command.force)) {
			if (command.reply_mutex != NULL) {
				command.reply_mutex->unlock();
			}
			return;
		}
		using std::chrono::days;
		using std::chrono::months;
		using std::chrono::years;
		year_month_day start_ymd = year_month_day(todays_date);
		local_days start_date = local_days(start_ymd), end_date;
		int length = 0, mod = 0;
		switch (command.contact.frequency) {
		case WEEKLY :
			end_date = start_date + days(7 - ((start_date - base_date).count() % 7));
			start_date = end_date - days(7);
			break;
		case BIWEEKLY :
			end_date = start_date + days(14 - ((start_date - base_date).count() % 14));
			start_date = end_date - days(14);
			break;
		case MONTHLY :
			start_date = local_days(start_ymd.year()/start_ymd.month()/1);
			end_date = local_days(year_month_day(start_date) + months(1));
			break;
		case BIMONTHLY : mod = 2; break;
		case QUARTERLY : mod = 3; break;
		case TRIANNUALLY : mod = 4; break;
		case SEMIANNUALLY : mod = 6; break;
		case ANNUALLY :
			start_date = local_days(start_ymd.year()/1/1);
			end_date = local_days(year_month_day(start_date) + years(1));
			break;
		default :
			length = -command.contact.frequency;
			end_date = todays_date + days(length - ((todays_date - base_date).count() % length));
			start_date = end_date - days(length);
			break;
		}
		if (mod > 0) {
			start_date = local_days(start_ymd.year()/(month_frequency_modifier(start_ymd.month(), mod, false) * mod + 1)/1);
			end_date = local_days(year_month_day(start_date) + months(mod));
		}
		if (start_date <= todays_date) {
			start_date = todays_date;
			if (!command.automatic && -command.contact.frequency != 1) {
				start_date += days(1);
			}
		}
		length = (end_date - start_date).count();
		if (length <= 0) {
			length = 1;
		}
		std::vector<unsigned> assignment_lengths = get_assignment_lengths(start_date, length);
		std::vector<unsigned> assignment_possibilities = get_assignment_possibilities(assignment_lengths);
		std::uniform_int_distribution uid(0, static_cast<int>(assignment_possibilities.size() - 1));
		unsigned index = assignment_possibilities[uid(mt)];
		local_days next_contact = start_date + days(index);
		assignments[next_contact].push_back(command.contact.get_uuid());
		command.contact.next_contact = year_month_day(next_contact);
		File_Manager::update_contact(&command.contact, false);
		if (!write_command_exists) {
			Command c; c.command = WRITE; Calendar_Manager::send_command(c);
			write_command_exists = true;
		}
		if (command.reply != NULL) {
			((Contact *)command.reply)->next_contact = year_month_day(next_contact);
		}
		if (command.reply_mutex != NULL) {
			command.reply_mutex->unlock();
		}
	}

	void reset_next_contact(Contact & contact, bool force, bool wait_for_return, bool automatic) {
		if (DEBUG) {
			Logging::log("Calendar_Manager::reset_next_contact(Contact & contact, bool wait_for_return)");
		}
		remove_contact(contact);
		Command c;
		c.command = RESET_NEXT_CONTACT;
		c.force = force;
		c.automatic = automatic;
		c.contact = Contact(contact); c.reply = (wait_for_return ? &contact : NULL);
		std::mutex reply_mutex; c.reply_mutex = (wait_for_return ? &reply_mutex : NULL);
		if (wait_for_return) {
			reply_mutex.lock();
		}
		send_command(c);
		if (wait_for_return) {
			reply_mutex.lock();
		}
	}

	void _remove_contact(Command command) {
		if (DEBUG) {
			Logging::log("Calendar_Manager::_remove_contact(Command command)");
		}
		string uuid = command.contact.get_uuid();
		std::vector<string> & uuids = assignments[local_days(command.contact.next_contact)];
		std::vector<string>::iterator it = uuids.begin();
		while (it != uuids.end() && *it != uuid) {
			it++;
		}
		if (it != uuids.end()) {
			uuids.erase(it);
		}
		if (!write_command_exists) {
			Command c; c.command = WRITE; Calendar_Manager::send_command(c);
			write_command_exists = true;
		}
	}

	void remove_contact(Contact & contact) {
		if (DEBUG) {
			Logging::log("Calendar_Manager::remove_contact(Contact & contact)");
		}
		Command c; c.contact = Contact(contact); c.command = REMOVE_CONTACT; send_command(c);
	}

	void _contact_today(Command command) {
		std::vector<string> uuids;
		todays_date = std::chrono::floor<std::chrono::days>(time_zone->to_local(std::chrono::system_clock::now()));
		std::map<local_days, std::vector<string>>::iterator it = assignments.find(todays_date);
		if (it != assignments.end()) {
			uuids = it->second;
		}
		*(std::vector<string> *)command.reply = uuids;
		command.reply_mutex->unlock();
		missed_contacts();
	}

	std::vector<string> contact_today() {
		Command c;
		c.command = CONTACT_TODAY;
		std::vector<string> reply; c.reply = &reply;
		std::mutex reply_mutex; c.reply_mutex = &reply_mutex;
		reply_mutex.lock();
		send_command(c);
		reply_mutex.lock();
		return reply;
	}

	void _quit() {
		if (DEBUG) {
			Logging::log("Calendar_Manager::_quit()");
		}
		if (write_command_exists) {
			_write();
		}
		throw NORMAL_EXIT;
	}

	// thread control
	void loop() {
		while (true) {
			Command current_command = get_command();
			switch (current_command.command) {
			case QUIT : _quit(); break;
			case WRITE : _write(); break;
			case SET_NEXT_CONTACT : _set_next_contact(current_command); break;
			case RESET_NEXT_CONTACT : _reset_next_contact(current_command); break;
			case REMOVE_CONTACT : _remove_contact(current_command); break;
			case CONTACT_TODAY : _contact_today(current_command); break;
			default : break;
			}
		}
	}

	void main() {
		try {
			check_files();
			todays_date = std::chrono::floor<std::chrono::days>(time_zone->to_local(std::chrono::system_clock::now()));
			read_assignments();
			missed_contacts();
			loop();
		} catch (const exception_flag_enum & exception_flag) {
			if (exception_flag != NORMAL_EXIT) {
				Network_Maintainer::throw_flag(exception_flag);
			}
			return;
		} catch (const std::exception & e) {
			Network_Maintainer::throw_exception(e);
			Logging::log(e.what());
			return;
		}
	}

	void start() {
		calendar_manager_thread = std::thread(main);
	}

	void join() {
		Logging::log("Calendar_Manager::join()");
		Command c; c.command = QUIT; Calendar_Manager::send_command(c);
		if (calendar_manager_thread.joinable()) {
			calendar_manager_thread.join();
		}
	}
}
