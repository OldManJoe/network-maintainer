//============================================================================
// Name        : Network_Maintainer.cpp
// Author      : Robert Higgins
// Version     : 2.1.1
// Copyright   : TBD
// Description : Application that sorts your network randomly over the next month in order to assist in keeping in contact with your network.
//============================================================================

#include "Network_Maintainer.h"
#include "Network_Maintainer_Contacts.h"
#include "Network_Maintainer_Social_Networks.h"
#include "Network_Maintainer_App.h"

#include <array>
#include <cctype>
#include <chrono>
#include <cmath>
#include <condition_variable>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <queue>
#include <random>
#include <regex>
#include <string>
#include <thread>
#include <vector>

#include "Calendar_Manager.h"
#include "Contact.h"
#include "File_Manager.h"
#include "Global_Variables.h"
#include "Logging.h"
#include "Social_Network.h"

using std::cin;
using std::cout;
using std::endl;
using std::flush;
using std::string;
using std::stringstream;
using std::to_string;

// Global Variables
bool DEBUG = false;
std::random_device rd;
std::mt19937_64 mt(rd());

namespace Network_Maintainer {
	// public variables

	// private enums, constants, structs, and classes
	enum network_maintainer_command_enum {PRINT, THROW_FLAG, THROW_EXCEPTION};
	enum menu_options_enum {CONTACT_TODAY, DISPLAY_NETWORK, FIND_PERSON, ADD_PERSON, ADD_PERSON_LIST, RESCHEDULE_ALL_CONTACTS, MODIFY_SOCIAL_NETWORKS, EDIT_SETTINGS, QUIT};
	const int MENU_OPTIONS_NUM = QUIT+1;
	const std::array<string, MENU_OPTIONS_NUM> MENU_OPTIONS_STRINGS = {"Contact Today's Network", "See Network List",
			"Find Person", "Add Person", "Add Contacts from List", "Force Reschedule All Contacts", "Modify Social Network List", "Edit Settings", "Quit"};

	struct Command {
		network_maintainer_command_enum command;
		string print;
		exception_flag_enum flag;
		std::exception e;
	};

	// private variables
	string settings_filename = "../settings.ini";
	bool GUI = true;

	// thread control
	std::queue<Command> command_queue;
	std::mutex queue_mutex;
	std::condition_variable not_empty;

	void flush_input() {
		cin.clear();
		cin.seekg(cin.end);
	}

	string clear_screen(int num_lines) {
		std::stringstream blank_lines;
		for (int aaa = 0; aaa < num_lines - 1; aaa++) {
			blank_lines << "\n";
		}
		blank_lines << endl;
		return blank_lines.str();
	}

	void _quit() {
		Logging::log("Network_Maintainer::_quit()");
		throw NORMAL_EXIT;
	}

	void read_settings_file() {
		bool file_type_set = false;
		string setting, line;
		std::ifstream settings_file(settings_filename);
		Logging::log("Network Maintainer Reading: " + settings_filename);
		if (!settings_file) { // could not open settings_file, write new settings file with default settings
			std::ofstream settings_file_write(settings_filename);
			settings_file_write << "file_type csv ;" << endl;
			settings_file.open(settings_filename);
		}
		while (settings_file) {
			settings_file >> setting;
			if (!settings_file) { break;
			}
			// settings_file.get() is called to get and discard the space between a setting and it's argument
			if (setting == "file_type") {
				settings_file.get(); getline(settings_file, line);
				File_Manager::set_file_type(line);
				file_type_set = true;
			} else if (setting == "time_zone") {
				settings_file.get(); getline(settings_file, line);
				Calendar_Manager::set_time_zone(line);
			} else if (setting == "contacts") {
				settings_file.get(); getline(settings_file, line);
				File_Manager::contacts_filename = std::filesystem::path(line);
			} else if (setting == "social_networks") {
				settings_file.get(); getline(settings_file, line);
				File_Manager::social_networks_filename = std::filesystem::path(line);
			} else if (setting == "calendar") {
				settings_file.get(); getline(settings_file, line);
				Calendar_Manager::calendar_filename = std::filesystem::path(line);
			} else if (setting == "base_date") {
				settings_file >> setting;
				Calendar_Manager::set_base_date(setting);
			} else if (setting == "log_count") {
				settings_file >> setting;
				Logging::set_log_count(stoi(setting));
			} else if (DEBUG && setting == "DEBUG") {
				settings_file.get(); getline(settings_file, line);
				Logging::log(line);
			} else {
				Logging::log("FILE_EXCEPTION: Settings file corrupted, recommend delete " + settings_filename);
				throw FILE_EXCEPTION;
			}
		}
		if (!file_type_set) {
			std::stringstream log_output;
			log_output << "FILE_EXCEPTION: " << settings_filename << " does not contain a " << std::quoted("file_type") <<
					" line\ndefault is " << std::quoted("file_type csv ;");
			Logging::log(log_output.str());
			cout << log_output.str() << endl;
			throw FILE_EXCEPTION;
		}
	}

	void write_settings_file() {
		std::ofstream settings_file_write(settings_filename);
		settings_file_write << File_Manager::write_settings();
		settings_file_write << Calendar_Manager::write_settings();
		settings_file_write << Logging::write_settings();
	}

	void edit_time_zone() {
		std::vector<const std::chrono::time_zone *> filtered_zones;
		string time_zone_filter;
		std::stringstream re_s;
		std::regex re;
		unsigned index = 0, choice = 0;
		char letter = '\0';

		cout << clear_screen(10);

		while (true) {
			cout << "Current Time Zone: " << Calendar_Manager::get_time_zone()->name() << "\n" <<
					"Time zones names come from IANA Time Zone Database" << "\n" <<
					"Example Time Zones: Etc/UTC, EST, America/Indiana/Indianapolis, Etc/GMT-5, Europe/London, CET" << "\n" << endl <<
					"Search for time zone, an empty search will print the full list: ";

			getline(std::cin, time_zone_filter);

			re_s.str("");
			re_s << ".*" << time_zone_filter << ".*";
			re = std::regex(re_s.str(), std::regex::icase);
			filtered_zones.clear();
			for (const std::chrono::time_zone & tz : Calendar_Manager::get_tzdb().zones) {
				if (time_zone_filter == "" || std::regex_search(std::string(tz.name()), re)) {
					filtered_zones.push_back(&tz);
				}
			}

			if (filtered_zones.size() == 0) {
				cout << "\nNo time zones found with that filter\n\n";
				continue;
			}

			letter = '\0';
			while (letter != 'B') {
				index = 0;
				cout << clear_screen(5);
				for (const std::chrono::time_zone * tz : filtered_zones) {
					cout << ++index << ". " << tz->name() << endl;
				}
				cout << "Enter number for selection or [B]ack or [C]ancel: "; cin >> letter;
				if (std::isdigit(letter)) {
					cin.putback(letter); cin >> choice; flush_input();
					if (choice > 0 && choice <= index) {
						Calendar_Manager::set_time_zone(filtered_zones[choice-1]);
						return;
					}
				} else {
					flush_input();
					switch (letter) {
					case 'b' : letter = 'B'; case 'B' : break;
					case 'c' : case 'C' : return; break;
					default : break;
					}
				}
			}
		}
	}

	void edit_settings() {
		std::chrono::year_month_day base_date_ymd = Calendar_Manager::get_base_date_ymd();
		int choice = 0, helper = 0;
		while (true) {
			cout << clear_screen(10) << "What setting would you like to change?\n" <<
					"1. Base Date: " << static_cast<int>(base_date_ymd.year()) << "-" << static_cast<unsigned>(base_date_ymd.month()) <<
										"-" << static_cast<unsigned>(base_date_ymd.day()) << "\n" <<
					"2. Time Zone: " << Calendar_Manager::get_time_zone()->name() << "\n" <<
					"3. Back\n\n" <<
					"Enter number for selection: "; cin >> choice; flush_input();
			switch (choice) {
			case 1 :
				cout << "The base date is used for determining the start of weeks, biweeks, and custom intervals\n" <<
						"Enter the year : "; cin >> choice; helper += choice;
				cout << "Enter the month: "; cin >> choice; helper *= 100; helper += choice;
				cout << "Enter the day  : "; cin >> choice; helper *= 100; helper += choice;
				if (Calendar_Manager::set_base_date(to_string(helper))) {
					base_date_ymd = Calendar_Manager::get_base_date_ymd();
				} else {
					cout << "Invalid Date\n";
				}
				break;
			case 2 : edit_time_zone(); break;
			case 3 : write_settings_file(); return; break;
			default : break;
			}
		}
	}

	void contact_today() {
		std::vector<string> uuids = Calendar_Manager::contact_today(), greetings = File_Manager::get_greetings(),
				questions = File_Manager::get_questions();
		std::uniform_int_distribution greetings_uid(0, static_cast<int>(greetings.size() - 1));
		std::uniform_int_distribution questions_uid(0, static_cast<int>(questions.size() - 1));
		std::vector<Contact> todays_contacts;
		for (string uuid : uuids) {
			todays_contacts.push_back(File_Manager::get_contact(uuid));
		}
		unsigned count = todays_contacts.size();
		cout << clear_screen() << "[C]ontacted     [S]kip     [R]eschedule\n" <<
				"Enter [C]ontacted after you have reached out to this contact, this will reset their contact date\n" <<
				"Enter [S]kip to requeue this contact and reach out to them after reaching out to the rest of todays contacts\n" <<
				"Enter [R]eschedule to set a new contact date within their current contact period\n\n";
		char letter; std::vector<Contact> skipped; string line;
		while (!todays_contacts.empty()) {
			for (Contact contact : todays_contacts) {
				letter = 'N';
				while (letter == 'N') {
					cout << count << " remaining\n" <<
							contact.display() << "\n\n" <<
							"Prompt: " << (greetings[greetings_uid(mt)]) << " " << (questions[questions_uid(mt)]);
					cout << "\n[N]otes     [C]ontacted     [S]kip     [R]eschedule     [B]ack\n" <<
							"Enter letter for selection: "; cin >> letter; flush_input();
					switch (letter) {
					case 'n' : letter = 'N'; case 'N' :
						cout << "Notes will be replaced with what you enter here, copy and paste any notes you want to keep\n" <<
								"Notes: "; getline(cin, line); contact.notes = line;
						break;
					case 'c' : case 'C' : --count; Calendar_Manager::set_next_contact(contact, true, false, false); break;
					case 's' : case 'S' : skipped.push_back(contact); break;
					case 'r' : case 'R' : --count; Calendar_Manager::reset_next_contact(contact, false, false, false); break;
					case 'b' : case 'B' : return; break;
					default : letter = 'N'; break;
					}
					cout << clear_screen(10);
				}
			}
			todays_contacts = skipped;
			skipped.clear();
		}
	}

	int display_menu() {
		if (DEBUG) {
			Logging::log("Network_Maintainer::display_menu()");
		}
		cout << clear_screen();
		for (int aaa = 0; aaa < MENU_OPTIONS_NUM; aaa++) {
			cout << aaa+1 << ". " << MENU_OPTIONS_STRINGS[aaa] << "\n";
		}
		cout << "\nChoice: ";
		int menu_choice = 0; string input;
		getline(cin, input); stringstream(input) >> menu_choice; flush_input();
		return menu_choice - 1;
	}

	void test() {
		if (DEBUG) {
			Logging::log("Network_Maintainer::test()");
			cout << "test" << endl;
		} else {
			return;
		}
	}

	// Commands
	Command get_command() {
		Logging::log("Network_Maintainer::get_command()");
		std::unique_lock<std::mutex> lock(queue_mutex);
		not_empty.wait(lock, [](){ return !command_queue.empty();});
		Command current_command = command_queue.front(); command_queue.pop();
		return current_command;
	}

	void empty_queue(bool end_program = false) {
		if (DEBUG) {
			stringstream debug_output; debug_output << "Network_Maintainer::empty_queue(bool end_program = " << (end_program ? "True" : "False") << ")";
			Logging::log(debug_output.str());
		}
		std::unique_lock<std::mutex> lock(queue_mutex);
		while (!command_queue.empty()) {
			lock.unlock();
			Command command = get_command();
			lock.lock();
			if (end_program) {
				switch (command.command) {
				case PRINT : cout << command.print; Logging::log(command.print); break;
				case THROW_FLAG : case THROW_EXCEPTION : break;
				default : Logging::log("BROKEN_COMMAND: Network_Maintainer received a broken command, contact programmers to correct problem"); break;
				}
			} else {
				switch (command.command) {
				case PRINT : cout << command.print; Logging::log(command.print); break;
				case THROW_FLAG : throw command.flag; break;
				case THROW_EXCEPTION : throw command.e; break;
				default : Logging::log("BROKEN_COMMAND: Network_Maintainer received a broken command, contact programmers to correct problem"); throw BROKEN_COMMAND; break;
				}
			}
		}
	}

	void send_command(Command command) {
		if (DEBUG) {
			stringstream debug_output; debug_output << "Network_Maintainer::send_command(Command command = {" << command.command << ", stuff})";
			Logging::log(debug_output.str());
		}
		std::unique_lock<std::mutex> lock(queue_mutex);
		command_queue.push(command);
		not_empty.notify_one();
	}

	void print(string print) {
		Command c; c.command = PRINT; c.print = print; send_command(c);
	}

	void throw_flag(exception_flag_enum flag) {
		if (DEBUG) {
			stringstream debug_output;
			debug_output << "Network_Maintainer::throw_flag(exception_flag_enum flag = " << flag << ")";
			Logging::log(debug_output.str());
		}
		Command c; c.command = THROW_FLAG; c.flag = flag; send_command(c);
	}

	void throw_exception(const std::exception & e) {
		if (DEBUG) {
			stringstream debug_output;
			debug_output << "Network_Maintainer::throw_exception(const std::exception & e)";
			Logging::log(debug_output.str());
		}
		Command c; c.command = THROW_EXCEPTION; c.e = e; send_command(c);
	}

	// thread control
	void main_loop() {
		for(;;) {
			empty_queue();
			int menu_choice = display_menu();
			switch (menu_choice) {
			case CONTACT_TODAY : contact_today(); break;
			case DISPLAY_NETWORK : display_network(); break;
			case FIND_PERSON : find_person(); break;
			case ADD_PERSON : add_person(); break;
			case ADD_PERSON_LIST : add_persons_from_list(); break;
			case RESCHEDULE_ALL_CONTACTS : force_reschedule_all_contacts(); break;
			case MODIFY_SOCIAL_NETWORKS : modify_social_networks(); break;
			case EDIT_SETTINGS : edit_settings(); break;
			case QUIT : _quit(); break;
			default: break;
			}
		}
	}

	void program_arguments(const int argc, char * argv[]) {
		string arg_string;
		for (int argv_index = 1; argv_index < argc; argv_index++) {
			arg_string = argv[argv_index];
			if (arg_string.compare("-DEBUG") == 0) {
				std::stringstream line;
				DEBUG = true;
				line << "Network_Maintainer::program_arguments(const int argc = " << argc << ", char * argv[] =";
				for (int aaa = 0; aaa < argc; aaa++) {
					line << " " << argv[aaa];
				}
				line << ")";
				Logging::log(line.str());
			} else if (arg_string.compare("-NO_GUI") == 0) {
				GUI = false;
			}
		}
	}

	void start() {
		Logging::start();
		read_settings_file();
		File_Manager::start();
		Calendar_Manager::start();
	}

	int end_program(const exception_flag_enum & exception_flag) {
		if (DEBUG) {
			stringstream debug_output;
			debug_output << "Network_Maintainer::end_program(const exception_flag_enum & exception_flag = " << exception_flag << ")";
			Logging::log(debug_output.str());
		}
		Calendar_Manager::join();
		File_Manager::join();
		empty_queue(true);
		Logging::log("Program ended exit flag: " + to_string(exception_flag));
		Logging::join();
		empty_queue(true);
		return exception_flag;
	}
}

using namespace Network_Maintainer;
int main(int argc, char * argv[]) {
	try {
		if (argc > 1) {
			program_arguments(argc, argv);
		}
		start();
		test();

		if (GUI) {
			wxEntry();
		} else {
			main_loop(); // is NO_GUI
		}
	} catch (const exception_flag_enum & exception_flag) {
		return end_program(exception_flag);
	} catch (const std::exception & e) {
		std::cerr << e.what() << endl;
		Logging::log(e.what());
		return end_program(UNKNOWN_ERROR);
	}
	return end_program(NORMAL_EXIT);
}
