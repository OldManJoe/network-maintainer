/*
 * Calendar_Manager.h
 *
 *  Created on: Oct 12, 2022
 *      Author: Robert Higgins
 */

#ifndef CALENDAR_MANAGER_H_
#define CALENDAR_MANAGER_H_

#include <array>
#include <chrono>
#include <filesystem>
#include <string>
#include <vector>

#include "Contact.h"

namespace Calendar_Manager {
	enum frequncy_enum {CUSTOM, WEEKLY, BIWEEKLY, MONTHLY, BIMONTHLY, QUARTERLY, TRIANNUALLY, SEMIANNUALLY, ANNUALLY};
	const int FREQUENCY_NUM = ANNUALLY + 1;
	const std::array<std::string, FREQUENCY_NUM> FREQUENCY_STRINGS = {"Custom", "Weekly", "Biweekly",
			"Monthly", "Bimonthly", "Quarterly", "Triannually", "Semiannually", "Annually"};
	const std::array<std::string, FREQUENCY_NUM> FREQUENCY_STRINGS_OPTIONS = {"Custom number of days", "Once every week", "Once every two weeks",
			"Once every month", "Once every two months", "Once every three months", "Once every four months", "Once every six months",
			"Once every year"};

	extern std::filesystem::path calendar_filename;

	std::string write_settings();
	bool set_base_date(std::string date);
	bool set_time_zone(std::string time_zone);
	bool set_time_zone(const std::chrono::time_zone * time_zone);
	const std::chrono::time_zone * get_time_zone();
	const std::chrono::tzdb & get_tzdb();
	std::chrono::year_month_day get_base_date_ymd();
	std::chrono::year_month_day get_todays_date();

	void set_next_contact(Contact & contact, bool set_last_contact, bool force, bool wait_for_return);
	void reset_next_contact(Contact & contact, bool force, bool wait_for_return, bool automatic);
	void remove_contact(Contact & contact);
	std::vector<std::string> contact_today();

	void start();
	void join();
}

#endif /* CALENDAR_MANAGER_H_ */
