/*
 * Network_Maintainer_Social_Networks.h
 *
 *  Created on: Aug 5, 2023
 *      Author: oldma
 */

#ifndef SRC_NETWORK_MAINTAINER_SOCIAL_NETWORKS_H_
#define SRC_NETWORK_MAINTAINER_SOCIAL_NETWORKS_H_

#include <string>

#include "Social_Network.h"

namespace Network_Maintainer {
	void modify_social_networks();
	std::string set_social_network_choice();
}

#endif /* SRC_NETWORK_MAINTAINER_SOCIAL_NETWORKS_H_ */
