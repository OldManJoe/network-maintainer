/*
 * Contact.h
 *
 *  Created on: Jan 9, 2023
 *      Author: Robert Higgins
 */

#ifndef CONTACT_H_
#define CONTACT_H_

#include <chrono>
#include <string>

class Contact {
private:
	std::string uuid; // Unique User ID

public:
	// TODO : add multiple name fields
	enum members_enum {UUID, NEXT_CONTACT, ENABLED, FIRST_NAME, MIDDLE_NAME, LAST_NAME, USERNAME, SNID, FREQUENCY, LAST_CONTACT, NOTES};
	static const int MEMBER_NUM = NOTES+1;

	std::string first_name;
	std::string middle_name;
	std::string last_name;
	std::string username;
	int frequency;
	std::chrono::year_month_day last_contact;
	std::chrono::year_month_day next_contact;
	std::string snid; // Social Network ID
	bool enabled;
	bool missed_contact;
	std::string notes;

	// methods
	Contact();
	Contact * open(std::string UUID);
	Contact copy();
	void set_uuid();
	void set_uuid(std::string nuid);
	std::string get_uuid() const;
	std::string get_name() const;
	std::string get_social_network_name() const;
	std::string display() const;
	std::string display_short() const;

};


#endif /* CONTACT_H_ */
